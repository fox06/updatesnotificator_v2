﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServerCompact;

namespace Upd.Data
{
    public class UpdNotificatorDbConfig : DbConfiguration
    {
        public UpdNotificatorDbConfig()
        {
            const string strAppDir = @"d:\";

            var sqlCeConnectionString = string.Format("Data Source={0}", @"d:\UpdDB.sdf");

            SetDefaultConnectionFactory(new SqlCeConnectionFactory(SqlCeProviderServices.ProviderInvariantName,
                strAppDir,
                sqlCeConnectionString));
            SetProviderServices(SqlCeProviderServices.ProviderInvariantName,
                SqlCeProviderServices.Instance);

            SetDatabaseInitializer(new CreateDatabaseIfNotExists<UpdNotificatorContext>());
        }
    }
}