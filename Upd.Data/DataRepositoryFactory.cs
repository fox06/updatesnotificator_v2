﻿using System;
using System.Collections.Generic;
using System.IO;
using Autofac;
using Core.Common;
using Core.Common.Contracts;
using Upd.Data.Contracts;
using Upd.Data.Repositories;

namespace Upd.Data
{
    public class DataRepositoryFactory : IDataRepositoryFactory
    {
        ContainerBuilder autofac;
        private IContainer container;

        public DataRepositoryFactory()
        {
            autofac = new ContainerBuilder();
            
            autofac.RegisterType<ProductRepository>().As<IProductsRepository>();
            autofac.RegisterType<ProductChangesRepository>().As<IProductChangesRepository>();
            autofac.RegisterType<VersionRepository>().As<IVersionsRepository>();
            autofac.RegisterType<VersionChangesRepository>().As<IVersionChangesRepository>();

            container = autofac.Build();

        }

        T IDataRepositoryFactory.GetDataRepository<T>()
        {

          return container.Resolve<T>();
           
        }
    }
}
