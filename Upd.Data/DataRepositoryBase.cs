﻿using Core.Common.Contracts;
using Core.Common.DBStore;

namespace Upd.Data
{
    public abstract class DataRepositoryBase<T> : DataRepositoryBase<T, UpdNotificatorContext>
         where T : class, IIdentifiableEntity, new()
    {
    }

}
