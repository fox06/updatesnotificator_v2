﻿using System;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Core.Common.Contracts;
using Upd.Business.Entities;

namespace Upd.Data
{

    public class UpdNotificatorContext : DbContext
    {
        public UpdNotificatorContext()
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<UpdNotificatorContext>());
        }

        public DbSet<Product> ProductSet { get; set; }
        public DbSet<Upd.Business.Entities.Version> VersionSet { get; set; }
        public DbSet<ProductChange> ProductChangesSet { get; set; }
        public DbSet<VersionChange> VersionChangesSet { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Ignore<PropertyChangedEventHandler>();
            modelBuilder.Ignore<IIdentifiableEntity>();

            modelBuilder.Entity<Product>().HasKey<Guid>(e => e.ProductId).Ignore(e => e.EntityId).Ignore(e => e.Versions);
            modelBuilder.Entity<Upd.Business.Entities.Version>().HasKey<Guid>(e => e.VersionId).Ignore(e => e.EntityId);
            modelBuilder.Entity<ProductChange>().HasKey<Guid>(e => e.ProductChangeId).Ignore(e => e.EntityId);
            modelBuilder.Entity<VersionChange>().HasKey<Guid>(e => e.VersionchangeId).Ignore(e => e.EntityId);

        }
    }
}
