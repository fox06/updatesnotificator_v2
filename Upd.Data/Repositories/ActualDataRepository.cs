using System;
using System.Collections;
using System.Collections.Generic;
using Upd.Business.Entities;
using Upd.Data.Contracts;

namespace Upd.Data
{
    public class ActualDataRepository : DataRepositoryBase<ActualDataRecord>, IActualDataRepository
    {
        protected override ActualDataRecord AddEntity(UpdNotificatorContext entityContext, ActualDataRecord entity)
        {
            throw new NotImplementedException();
        }

        protected override ActualDataRecord UpdateEntity(UpdNotificatorContext entityContext, ActualDataRecord entity)
        {
            throw new NotImplementedException();
        }

        protected override IEnumerable<ActualDataRecord> GetEntities(UpdNotificatorContext entityContext)
        {
            throw new NotImplementedException();
        }

        protected override ActualDataRecord GetEntity(UpdNotificatorContext entityContext, Guid id)
        {
            throw new NotImplementedException();
        }

        public ActualDataRecord Add(ActualDataRecord entity)
        {
            throw new NotImplementedException();
        }

        public void InsertOrUpdate(IEnumerable entities)
        {
            throw new NotImplementedException();
        }

        public void Remove(ActualDataRecord entity)
        {
            throw new NotImplementedException();
        }

        public ActualDataRecord Update(ActualDataRecord entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable Get()
        {
            throw new NotImplementedException();
        }

        public ActualDataRecord Get(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}