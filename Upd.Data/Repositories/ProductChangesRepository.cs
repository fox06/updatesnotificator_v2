using System;
using System.Collections.Generic;
using System.Linq;
using Upd.Business.Entities;
using Upd.Data.Contracts;

namespace Upd.Data
{
    public class ProductChangesRepository : DataRepositoryBase<ProductChange>, IProductChangesRepository
    {
        protected override ProductChange AddEntity(UpdNotificatorContext entityContext, ProductChange entity)
        {
            return entityContext.ProductChangesSet.Add(entity);
        }

        protected override ProductChange UpdateEntity(UpdNotificatorContext entityContext, ProductChange entity)
        {
            return (from e in entityContext.ProductChangesSet
                    where e.ProductChangeId == entity.ProductChangeId
                    select e).FirstOrDefault();
        }

        protected override IEnumerable<ProductChange> GetEntities(UpdNotificatorContext entityContext)
        {
            return from e in entityContext.ProductChangesSet
                   select e;
        }

        protected override ProductChange GetEntity(UpdNotificatorContext entityContext, Guid id)
        {
            var query = (from e in entityContext.ProductChangesSet
                         where e.ProductChangeId == id
                         select e);

            var results = query.FirstOrDefault();

            return results;
        }

       

    }
}