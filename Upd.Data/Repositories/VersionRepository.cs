﻿using System;
using System.Collections.Generic;
using System.Linq;
using Upd.Business.Entities;
using Upd.Data.Contracts;
using Version = Upd.Business.Entities.Version;


namespace Upd.Data.Repositories
{
    public class VersionRepository : DataRepositoryBase<Version>, IVersionsRepository
    {
        protected override Version AddEntity(UpdNotificatorContext entityContext, Version entity)
        {
            return entityContext.VersionSet.Add(entity);
        }

        protected override Version UpdateEntity(UpdNotificatorContext entityContext, Version entity)
        {
            var vers = (from e in entityContext.VersionSet
                where e.VersionId == entity.VersionId
                select e).FirstOrDefault();

            return vers;
        }

        protected override IEnumerable<Version> GetEntities(UpdNotificatorContext entityContext)
        {
            return from e in entityContext.VersionSet
                select e;
        }

        protected override Version GetEntity(UpdNotificatorContext entityContext, Guid id)
        {
            var query = (from e in entityContext.VersionSet
                where e.VersionId == id
                select e);

            var results = query.FirstOrDefault();

            return results;
        }
        
        
        //public List<ProductVersion> GetAllUpToDateVersions()
        //{
        //    using (var entityContext = new UpdNotificatorContext())
        //    {
        //        var s =
        //            (from product in entityContext.ProductSet where product.CheckUpdates select product.Id).ToArray();


        //        var t = entityContext.VersionSet.Where(p => s.Contains(p.Id))
        //    .GroupBy(pv => new { pv.Id, pv.productCode,  pv.VersionType })
        //    .Select(g => g.OrderByDescending(pv => pv.Date).FirstOrDefault())
        //    .ToList();
               
                              

        //        return t;
        //    }
        //}


        public IEnumerable<Version> GetOnlyActualVersionsByProductId(Guid productdId)
        {
            using (UpdNotificatorContext context = new UpdNotificatorContext())
            {
                var query = (from e in context.VersionSet where e.ProductId == productdId & e.IsActual select e).ToArray().ToList();

                return query;
            }


        }

        public IEnumerable<Version> GetOnlyActualVersionsByProductId(Guid productdId, VersionTypes versionType)
        {
            using (UpdNotificatorContext context = new UpdNotificatorContext())
            {
                var query = (from e in context.VersionSet where e.ProductId == productdId && e.VersionType == versionType && e.IsActual select e).ToArray().ToList();

                return query;
            }

        }
    }
}