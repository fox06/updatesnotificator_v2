﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Core.Common;
using Upd.Business.Entities;
using Upd.Data.Contracts;
using Upd.Data.Contracts.DTO;

namespace Upd.Data
{
    public class ProductRepository : DataRepositoryBase<Product>, IProductsRepository
    {
        protected override Product AddEntity(UpdNotificatorContext entityContext, Product entity)
        {
            return entityContext.ProductSet.Add(entity);
        }

        protected override Product UpdateEntity(UpdNotificatorContext entityContext, Product entity)
        {
            return (from e in entityContext.ProductSet
                    where e.ProductId == entity.ProductId
                    select e).FirstOrDefault();
        }

        protected override IEnumerable<Product> GetEntities(UpdNotificatorContext entityContext)
        {
            return from e in entityContext.ProductSet
                   select e;
        }

        protected override Product GetEntity(UpdNotificatorContext entityContext, Guid id)
        {
            var query = (from e in entityContext.ProductSet
                         where e.ProductId == id
                         select e);

            var results = query.FirstOrDefault();

            return results;
        }
        
        public Product GetByProductCode(string productCode)
        {
            using (var entityContext = new UpdNotificatorContext())
            {
                return (from e in entityContext.ProductSet
                        where e.ProductCode == productCode
                        select e).FirstOrDefault();
            }
        }

        public IEnumerable<ProductDTO> GetAllProductsWithVersions()
        {
            using (var entityContext = new UpdNotificatorContext())
            {

                var query = from r in entityContext.ProductSet
                                join a in entityContext.VersionSet on r.ProductId equals a.ProductId into productVersions
                                select new ProductDTO()
                                {
                                    EntityId = r.ProductId,
                                    Name = r.Name,
                                    Platform = r.Platform,
                                    ProductCode = r.ProductCode,
                                    Versions = productVersions.ToList(),
                                   
                                };

              
                return query.ToArray().ToList();
            }

        
        }
    }
}
