using System;
using System.Collections.Generic;
using System.Linq;
using Upd.Business.Entities;
using Upd.Data.Contracts;

namespace Upd.Data
{
    public class VersionChangesRepository : DataRepositoryBase<VersionChange>, IVersionChangesRepository
    {
        protected override VersionChange AddEntity(UpdNotificatorContext entityContext, VersionChange entity)
        {
            return entityContext.VersionChangesSet.Add(entity);
        }

        protected override VersionChange UpdateEntity(UpdNotificatorContext entityContext, VersionChange entity)
        {
            return (from e in entityContext.VersionChangesSet
                where e.VersionchangeId == entity.VersionchangeId
                select e).FirstOrDefault();
        }

        protected override IEnumerable<VersionChange> GetEntities(UpdNotificatorContext entityContext)
        {
            return from e in entityContext.VersionChangesSet
                select e;
        }

        protected override VersionChange GetEntity(UpdNotificatorContext entityContext, Guid id)
        {
            var query = (from e in entityContext.VersionChangesSet
                where e.VersionchangeId == id
                select e);

            var results = query.FirstOrDefault();

            return results;
        }
        
    }
}