﻿using System.Collections.Generic;
using Core.Common.Contracts;
using Version = Upd.Business.Entities.Version;

namespace Upd.Parser.Contracts
{
    public interface IVersionPageParserV8 : IDataParser<ICollection<Version>>
    {
        
    }
}
