﻿using System.Collections.Generic;
using Core.Common.Contracts;
using Upd.Business.Entities;

namespace Upd.Parser.Contracts
{
    public interface ISummaryPageParser : IDataParser<ICollection<Product>>
    {

    }
}