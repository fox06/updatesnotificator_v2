﻿using Autofac;
using Core.Common;
using Core.Common.Contracts;
using Upd.Parser.Contracts;

namespace Upd.Parser
{

    public class DataParsersFactory : IDataParsersFactory
    {
       ContainerBuilder autofac;
       private IContainer container;

        public DataParsersFactory()
        {
             autofac = new ContainerBuilder();

            autofac.RegisterType<V8SummaryPageParser>().As<ISummaryPageParserV8>();
            autofac.RegisterType<V7SummaryPageParser>().As<ISummaryPageParserV7>();
            
            container = autofac.Build();
        }
        T IDataParsersFactory.GetDataParser<T>()
        {
            return container.Resolve<T>();
        }


      
    }
}
