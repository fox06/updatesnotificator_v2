﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Core.Common.Contracts;
using HtmlAgilityPack;
using Upd.Business.Entities;
using Upd.Parser.Contracts;
using Version = Upd.Business.Entities.Version;

namespace Upd.Parser
{
    public class V7SummaryPageParser : ServerDataParserBase<ICollection<Product>>, ISummaryPageParserV7
    {
        public override ICollection<Product> Parse(string incomeData, string validationKey)
        {
          //  var doc = new HtmlDocument();
           // incomeData = HtmlEntity.DeEntitize(incomeData);
            HtmlDocument.LoadHtml(incomeData);
            var objectCollection = new List<Product>();

            var nodes = HtmlDocument.DocumentNode.SelectNodes("//table[@class = 'content']/tr");

            if (nodes == null)
                return null;

            //Поиск всех нодов HREF содержащих ссылки на страницы конфигураций
            //  foreach (HtmlAgilityPack.HtmlNode link in doc.DocumentNode.SelectNodes("//*[contains(concat(' ', normalize-space(@href), ' '), 'project')]"))
            //  foreach (HtmlAgilityPack.HtmlNode link in doc.DocumentNode.SelectNodes("//td[@class = 'em']"))
            string name = String.Empty;
            foreach (HtmlNode tableRow in nodes)
            {
                if (tableRow.SelectNodes("./td") == null)
                    continue;


                if (tableRow.SelectNodes("./td").Count == 1)
                {
                    name = tableRow.InnerText.Trim();
                    continue;
                }

                if (!tableRow.SelectNodes("./td")[2].InnerText.Contains("7.70"))
                    continue;

                var product = BuildProduct(name, tableRow);
                var version = BuildProductVersion(product, tableRow);
                product.AddVersion(version);

               // foreach (HtmlNode item in tableRow.SelectNodes(".//a[@href]"))
               // {
               //     HtmlAttribute attrib = item.Attributes["href"];

               ////     string link = "techsupp.1c.ru" + attrib.Value.Trim();
               // //    string discription = item.InnerText.Trim();

               //     //dataCollection.Content.Add(new Content(product.Id, link, discription));
               // }

                objectCollection.Add(product);

                //dataCollection.ProductsList.Add(product);
                //dataCollection.VersionsList.Add(version);
            }

            return objectCollection;
        }


        private Product BuildProduct(string name, HtmlNode tableRow)
        {
            string pastName = tableRow.SelectNodes("./td/span")[1].InnerText.Trim();

            string fullName = String.Concat(name, " ", pastName);

            var product = new Product();
            product.ProductCode = fullName.GetHashCode().ToString();
            product.Name = fullName;
            product.Platform = Platform.V7;
                
            return product;
        }

        private Version BuildProductVersion(Product product, HtmlNode tableRow)
        {
            DateTime releaseTime = DateTime.Parse(tableRow.SelectNodes("./td/span")[0].InnerText);

            if (releaseTime == DateTime.MinValue)
            {
                releaseTime = DateTime.Parse("1753-1-1");
            }



            var currentVersion = new Version
            {
                VersionNumber = tableRow.SelectNodes("./td/span")[2].InnerText,
                VersionType = VersionTypes.Release,
                Date = releaseTime,
                ProductId = product.EntityId
            };

            return currentVersion;
        }

        

     
    }
}
