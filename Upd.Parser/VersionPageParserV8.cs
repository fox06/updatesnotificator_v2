﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;
using Upd.Business.Entities;
using Upd.Parser.Contracts;
using Upd.Parser.Exceptions;
using Version = Upd.Business.Entities.Version;

namespace Upd.Parser
{
    public class V8VersionPageParser : ServerDataParserBase<ICollection<Version>>, IVersionPageParserV8
    {
        public override ICollection<Version> Parse(string incomeData, string validationKey)
        {
            HtmlDocument.LoadHtml(incomeData);

            try
            {
                if (!Validate(incomeData, validationKey))
                    throw new IncomeDataValidationFailed(String.Format("Key '{0}' not found in", validationKey));


                string productName =
                    HtmlDocument.DocumentNode.SelectSingleNode(@"//div[@class='formLine']/descendant::h2").InnerText;

                var objectCollection = new List<Version>();

                ExtractLastVersion(HtmlDocument, objectCollection);
                ExtractPlannedVersion(HtmlDocument, objectCollection);
                ExtractTestsVersions(HtmlDocument, objectCollection);
                ExtractAddaditioonFiles(HtmlDocument, objectCollection);

                return objectCollection;
            }
            catch (IncomeDataValidationFailed)
            {
                throw;
            }
            catch (Exception)
            {
                throw new UnsupportedIncomeDataFormat(String.Format("HTML page has unsupport format"));
            }
            finally
            {
                HtmlDocument = null;
            }

        }


        private void ExtractLastVersion(HtmlDocument document, ICollection<Version> objectCollection)
        {
            HtmlNode node =
                document.DocumentNode.SelectSingleNode(
                    @"//div[@class='formLine']/descendant::table[@id='versionsTable']//tr[2]");
            if (node == null)
                return;

            string version =
                ParserHelper.RemoveTagsAndTrim(node.SelectSingleNode("//td[@class='versionColumn']").InnerText);
            string versionDate =
                ParserHelper.RemoveTagsAndTrim(node.SelectSingleNode("//td[@class='dateColumn']").InnerText);
            // var itsColumn = ParserHelper.RemoveTagsAndTrim(node.SelectSingleNode("//td[@class='itsColumn']").InnerText);
            string previousVersions =
                ParserHelper.RemoveTagsAndTrim(
                    node.SelectSingleNode("//td[@class='version previousVersionsColumn']").InnerText);

            List<string> previousVersionsList = previousVersions.Split(',').Select(p => p.Trim()).ToList();

            var ver = new Version(Guid.NewGuid(), version, VersionTypes.Release, DateTime.Parse(versionDate));
            //previousVersionsList

            objectCollection.Add(ver);
        }

        private void ExtractPlannedVersion(HtmlDocument document, ICollection<Version> objectCollection)
        {
            //var node = document.DocumentNode.SelectSingleNode(@"//div[@class='formLine']/descendant::table[@class='planTable']//td[not(@*)]");
            HtmlNodeCollection nodes = document.DocumentNode.SelectNodes(@"//table[@class='planTable']");

            if (nodes == null)
                return;

            HtmlNodeCollection discriptionNodes =
                document.DocumentNode.SelectNodes(
                    @"//div[@class='formLine']/descendant::table[@class='planTable']/following::div[1]");

            foreach (HtmlNode node in nodes)
            {
                //var version = ParserHelper.RemoveTagsAndTrim(node.SelectNodes("./tbody/tr/td[not(@*)]")[0].InnerText);
                string version = ParserHelper.RemoveTagsAndTrim(node.SelectNodes("./tr/td[not(@*)]")[0].InnerText);
                string versionDate = ParserHelper.RemoveTagsAndTrim(node.SelectNodes("./tr/td[not(@*)]")[1].InnerText);

                int index = nodes.IndexOf(node);
                string discription = "";
                if (index + 1 <= discriptionNodes.Count)
                {
                    discription = discriptionNodes[index].InnerHtml.Replace("<br>", "\r\n").Trim();
                }

                var ver = new Version(Guid.NewGuid(), version, VersionTypes.Planned, DateTime.Parse(versionDate));
                //discription

                objectCollection.Add(ver);
            }
        }

        private void ExtractTestsVersions(HtmlDocument document, ICollection<Version> objectCollection)
        {
            HtmlNodeCollection nodes =
                document.DocumentNode.SelectNodes(
                    @"//div[@class='formLine']/h5[contains(text(), 'Верс')]/following::p[.//a]");
            if (nodes == null)
                return;
            foreach (HtmlNode node in nodes)
            {
                string version = ParserHelper.RemoveTagsAndTrim(node.SelectSingleNode("./a").InnerText);
                string versionDate = ParserHelper.RemoveTagsAndTrim(node.InnerText).Substring(0, 8);

                var ver = new Version(Guid.NewGuid(), version, VersionTypes.Preview, DateTime.Parse(versionDate));
                //discription

                objectCollection.Add(ver);


               // objectCollection.AddTestVersion(version, DateTime.Parse(versionDate), "");
            }
        }


        private void ExtractAddaditioonFiles(HtmlDocument document, ICollection<Version> objectCollection)
        {
            HtmlNodeCollection nodes =
                document.DocumentNode.SelectNodes(
                    @"//div[@class='formLine']/descendant::table[@id='additionalFilesTable']//td[@class='additionalFile']//a");

            if (nodes == null)
                return;


            foreach (HtmlNode node in nodes)
            {
                string href = HtmlEntity.DeEntitize(node.Attributes[0].Value);
                string discription = ParserHelper.RemoveTagsAndTrim(node.InnerText);
               // objectCollection.AddAddiditionalContent(new Uri(KeyUri, href), discription);
            }
        }

        
    }
}
