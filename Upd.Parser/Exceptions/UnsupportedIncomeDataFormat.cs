using System;

namespace Upd.Parser.Exceptions
{
    public class UnsupportedIncomeDataFormat : ApplicationException
    {
        public UnsupportedIncomeDataFormat(string message)
            : base(message)
        {
        }

        public UnsupportedIncomeDataFormat(string message, Exception ex)
            : base(message, ex)
        {
        }
    }
}