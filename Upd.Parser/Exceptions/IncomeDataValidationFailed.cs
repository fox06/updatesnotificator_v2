using System;

namespace Upd.Parser.Exceptions
{
    public class IncomeDataValidationFailed : ApplicationException
    {
        public IncomeDataValidationFailed(string message)
            : base(message)
        {
        }

        public IncomeDataValidationFailed(string message, Exception ex)
            : base(message, ex)
        {
        }
      
    }
}