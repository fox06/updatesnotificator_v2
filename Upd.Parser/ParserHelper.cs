﻿using System;
using System.Text.RegularExpressions;

namespace Upd.Parser
{
    public static class ParserHelper
    {
        public static string RemoveTagsAndTrim(string text)
        {
            return Regex.Replace(text.Trim(), @"\t|\n|\r|&nbsp;", "");
        }

        public static DateTime ParseStringToDateTime(string stringDate)
        {
            DateTime date;
            if (DateTime.TryParse(stringDate, out date))
            {
                return date;
            }

            return DateTime.MinValue;
        }
    }
}