﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using Upd.Business.Entities;
using Upd.Parser.Contracts;
using Version = Upd.Business.Entities.Version;

namespace Upd.Parser
{
    public class V8SummaryPageParser : ServerDataParserBase<ICollection<Product>>, ISummaryPageParserV8
    {
      
        public override ICollection<Product> Parse(string incomeData, string validationKey)
        {
            HtmlDocument.LoadHtml(incomeData);

            var objectCollection = new List<Product>();

            ExtractAvailableProducts(objectCollection);

            return objectCollection;
        }

        private void ExtractAvailableProducts(ICollection<Product> objectCollection)
        {
            HtmlNodeCollection nodes =
                HtmlDocument.DocumentNode.SelectNodes(@"//table[@id='actualTable']//following::tr[./td[./a]]");

            if (nodes == null)
                return;

            var regEx = new Regex(@"(?<=project/)\w+");
            //(?<=project/)\S+(?=/)

            foreach (HtmlNode node in nodes)
            {
                var columns = node.SelectNodes("./td");
                var hrefNode = columns[0].SelectSingleNode("./a");
                string id = hrefNode.HasAttributes ? regEx.Match(hrefNode.Attributes[0].Value).Value : "";
                string name = ParserHelper.RemoveTagsAndTrim(columns[0].InnerText);
                //string releaseVerNumber = ParserHelper.RemoveTagsAndTrim(columns[1].InnerText);
                //string releaseDate = ParserHelper.RemoveTagsAndTrim(columns[2].InnerText);

                var row = new Product();
                row.Name = name;
                row.ProductCode = id;
                //   row.AddReleaseVersion(releaseVerNumber, ParserHelper.ParseStringToDateTime(releaseDate));
                //   var previewVersionsNodes = columns[6].SelectNodes("./a");


                ExtractVersionsData(columns, "./a", "./@class", 1, row, VersionTypes.Release);
                ExtractVersionsData(columns, "./span", "./span", 3, row, VersionTypes.Planned);
                ExtractVersionsData(columns, "./a", "./span", 6, row, VersionTypes.Preview);






                objectCollection.Add(row);
            }
        }

        public void ExtractVersionsData(HtmlNodeCollection row, string versionTag, string dateTag, int versionColumnNumber, Product summaryRow, VersionTypes versionType)
        {
            var versionsNodes = row[versionColumnNumber].SelectNodes(versionTag);
            if (versionsNodes == null) return;
            for (int i = 0; i < versionsNodes.Count; i++)
            {
                var versionDateNodes = row[versionColumnNumber + 1].SelectNodes(dateTag);

                string verDate = ParserHelper.RemoveTagsAndTrim(versionDateNodes[i].InnerText);
                string ver = ParserHelper.RemoveTagsAndTrim(versionsNodes[i].InnerText);

                var version = new Version();
                version.VersionNumber = ver;
                version.Date = ParserHelper.ParseStringToDateTime(verDate);
                version.VersionType = versionType;

                summaryRow.AddVersion(version);

               // summaryRow.AddVersion(ver, ParserHelper.ParseStringToDateTime(verDate), versionType);


            }
        }

    }
}