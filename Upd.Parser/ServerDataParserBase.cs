﻿using System;
using System.Collections.Generic;
using HtmlAgilityPack;
using Version = Upd.Business.Entities.Version;

namespace Upd.Parser
{
    public abstract class ServerDataParserBase<T>  
    {
        protected HtmlDocument HtmlDocument;
  

        protected ServerDataParserBase()
        {
            HtmlDocument = new HtmlDocument();
        }


        public abstract T Parse(string incomeData, string validationKey);

        public virtual bool Validate(string incomeData, string validationKey)
        {
            return incomeData.IndexOf(validationKey, StringComparison.OrdinalIgnoreCase) != -1;
        }
    }
}
