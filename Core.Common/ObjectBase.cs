﻿using System.ComponentModel.Composition.Hosting;

namespace Core.Common
{
    public abstract class ObjectBase
    {
        public static CompositionContainer Container { get; set; }
    }
}
