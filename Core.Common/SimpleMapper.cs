﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Core.Common
{
    public static class SimpleMapper
    {
        public static void PropertyMap<T, TU>(T source, TU destination)
            where T : class
            where TU : class
        {
            List<PropertyInfo> sourceProperties = source.GetType().GetProperties().ToList();
            List<PropertyInfo> destinationProperties = destination.GetType().GetProperties().Where(p => p.GetCustomAttributes(typeof(UnCopyableAttribute), true).Length == 0).ToList();

            foreach (PropertyInfo sourceProperty in sourceProperties)
            {
                PropertyInfo destinationProperty = destinationProperties.Find(item => item.Name == sourceProperty.Name);

                if (destinationProperty != null)
                {
                    try
                    {
                        destinationProperty.SetValue(destination, sourceProperty.GetValue(source, null), null);
                    }
                    catch (ArgumentException)
                    {
                    }
                }
            }
        }
    }

    public class UnCopyableAttribute : Attribute
    {
    }
}
