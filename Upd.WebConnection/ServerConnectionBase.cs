﻿using System;
using System.IO;
using System.Net;
using System.Text;
using Core.Common.Contracts;
using Upd.WebConnection.Contracts;

namespace Upd.WebConnection
{
    public abstract class ServerConnectionBase : IServerConnection 
    {
        protected ServerConnectionBase(int timeOut = 5000)
        {
            this.Encoding = Encoding.UTF8;
            Timeout = timeOut;
        }

        public Encoding Encoding { get; protected set; }

        public IWebProxy Proxy { get; set; }

        public int Timeout { get; set; }

        #region Properties

        private long _contentLength;
        private string _contentType;
        private DateTime _lastModified;
        private Uri _responseUri;
        private HttpStatusCode _statusCode;

        public string ContentType
        {
            get { return _contentType; }
        }

        public long ContentLength
        {
            get { return _contentLength; }
        }

        public DateTime LastModified
        {
            get { return _lastModified; }
        }

        public HttpStatusCode StatusCode
        {
            get { return _statusCode; }
        }

        public Uri ResponseUri
        {
            get { return _responseUri; }
        }

        public AuthDataSourceBase AuthDataSource { get; set; }

        #endregion

        protected abstract HttpWebRequest BuildWebReqest(Uri link);

        public HttpWebResponse GetHttpWebResponse(Uri link)
        {
            return (HttpWebResponse)BuildWebReqest(link).GetResponse();
        }

        public string DownloadString(Uri uri)
        {
            if (uri == null)
                throw new ArgumentNullException("uri");

            WebRequest request = BuildWebReqest(uri);
            
            request.Timeout = Timeout;

            using (WebResponse resp = request.GetResponse())
                return DownloadString(resp);
        }

        private string DownloadString(WebResponse resp)
        {
            CheckWebResponse(resp);

            using (Stream stream = resp.GetResponseStream())
            {
                using (var sr = new StreamReader(stream, Encoding))
                    return sr.ReadToEnd();
            }
        }


        public bool IsUrlHasRespond(Uri uri)
        {
            if (uri == null)
                throw new ArgumentException("uri");

            WebRequest request = BuildWebReqest(uri);

            request.Method = "HEAD";

            if (Proxy != null)
                request.Proxy = Proxy;

            request.Timeout = Timeout;

            try
            {
                using (WebResponse resp = request.GetResponse())
                {
                    CheckWebResponse(resp);
                    if (StatusCode == HttpStatusCode.OK || StatusCode == HttpStatusCode.Found)
                        return true;
                }
            }
            catch (WebException)
            {
                return false;
            }

            return false;
        }

        public void CheckWebResponse(WebResponse resp)
        {
            _responseUri = resp.ResponseUri;
            _contentType = resp.ContentType;
            //html loaded from disc has octet-stream so we reset it
            if (_contentType.Equals("application/octet-stream") && _responseUri.AbsolutePath.EndsWith(".html"))
                _contentType = "text/html";
            _contentLength = resp.ContentLength;
            //files don't return HttpStatusCode
            _statusCode = HttpStatusCode.Unused;
            if (ContentLength > 0) _statusCode = HttpStatusCode.OK;

            var httpResp = resp as HttpWebResponse;

            if (httpResp != null)
            {
                _statusCode = httpResp.StatusCode;
                _lastModified = httpResp.LastModified;
            }
        }

        public abstract bool Authorization();
        public abstract string GetProductsTotalPage();
    }
}
