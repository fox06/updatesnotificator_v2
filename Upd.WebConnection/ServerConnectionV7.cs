﻿using System;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using Upd.WebConnection.Contracts;

namespace Upd.WebConnection
{
    class ServerConnectionV7 : ServerConnectionBase, IServerConnectionV7
    {
        private CredentialCache _credentialCache;

       
        public override bool Authorization()
        {
            return CheckConnection();
        }

        public override string GetProductsTotalPage()
        {
            return DownloadString(GetProductsUri());
        }

        private bool CheckConnection()
        {
            var request = BuildWebReqest(AuthDataSource.LoginUri);

            try
            {
                using (request.GetResponse()) { }
            }
            catch (WebException)
            {
                return false;
            }

            return true;
        }

        protected override HttpWebRequest BuildWebReqest(Uri link)
        {
            var request = (HttpWebRequest)WebRequest.Create(link);

            var headers = new NameValueCollection
            {
                {"User-Agent", "Mozilla/5.0"},
                {"Content-Type", "application/x-www-form-urlencoded"}
            };

            request.UserAgent = headers["User-Agent"];
            request.ContentType = headers["Content-Type"];
            request.Method = "GET";
            request.KeepAlive = true;
            request.Credentials = _credentialCache;

            return request;
        }

        public Uri GetProductsUri()
        {
            return new Uri(AuthDataSource.BaseUri + "/categ.jsp?GroupID=88");
        }

        public Uri GetReportsUri()
        {
            //TODO: implement report page return
            return null;
        }

        public void SetAuthorizationDataSource(AuthDataSourceBase authDataSource)
        {
            this.AuthDataSource = authDataSource;
            this.Encoding = Encoding.GetEncoding("Windows-1251");

            var networkCredential = new NetworkCredential(authDataSource.Login, authDataSource.Password);
            _credentialCache = new CredentialCache { { authDataSource.LoginUri, "BASIC", networkCredential } };
        }
    }
}