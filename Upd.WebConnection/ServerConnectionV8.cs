﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using HtmlAgilityPack;
using Upd.WebConnection.Contracts;

namespace Upd.WebConnection
{
    public class ServerConnectionV8 : ServerConnectionBase, IServerConnectionV8
    {
        private string _jsoncode;
        Uri _productsTotalPage;

        
        public override bool Authorization()
        {
            try
            {
                var webRequest1 = (HttpWebRequest)WebRequest.Create(AuthDataSource.LoginUri);
                webRequest1.UserAgent = "1C+Enterprise/8.3";
                webRequest1.KeepAlive = true;

                var webRequest2 = (HttpWebRequest)WebRequest.Create(AuthDataSource.LoginUri);
                webRequest2.UserAgent = "User-Agent: 1C+Enterprise/8.3";
                // webRequest2.UserAgent = "1C+Enterprise/8.3";
                webRequest2.KeepAlive = true;
                webRequest2.ContentType = "application/x-www-form-urlencoded";

                string jsessionid1;
                string str;

                using (var webResponse = (HttpWebResponse)webRequest1.GetResponse())
                {
                    //var webResponse = (HttpWebResponse)webRequest1.GetResponse();


                    using (Stream stream = webResponse.GetResponseStream())
                    {
                        using (var reader = new StreamReader(stream))
                        {
                            str = reader.ReadToEnd();
                            reader.Close();
                           // jsessionid1 = webResponse.Headers["Set-Cookie"].Substring(92,
                           //     webResponse.Headers["Set-Cookie"].IndexOf(";", StringComparison.Ordinal));

                            jsessionid1 = webResponse.Headers["Set-Cookie"].Substring(92, 36);
                           // cokk = webResponse.Headers["Set-Cookie"];

                        }
                    }
                }

             
                string inviteCode = GetInviteCode(str);
                inviteCode += "&username=" + AuthDataSource.Login + "&password=" + AuthDataSource.Password;


                webRequest2.Headers.Add("Cookie", jsessionid1);
                webRequest2.Method = "Post";
                webRequest2.Referer = "https://login.1c.ru/login";
                webRequest2.Accept = "text/html, application/xhtml+xml, */*";
                webRequest2.KeepAlive = true;
                webRequest2.Headers.Add("DNT", "1");
                webRequest2.Headers.Add("Accept-Encoding", "gzip, deflate");
                webRequest2.Headers.Add("Cookie", "client_language=ru_RU; i18next=ru-RU; SESSION=" + jsessionid1);

                var encoding = new ASCIIEncoding();
                byte[] data = encoding.GetBytes(inviteCode);
                using (Stream newStream = webRequest2.GetRequestStream())
                {
                    newStream.Write(data, 0, data.Length);
                    newStream.Close();
                }

                webRequest2.AllowAutoRedirect = false;

                string onecSecurity;
                string locationHeaderValue;

                using (var webResponse = (HttpWebResponse)webRequest2.GetResponse())
                {
                    onecSecurity = webResponse.Headers["Set-Cookie"];
                    locationHeaderValue = webResponse.Headers["Location"];
                }

                int pos = onecSecurity.IndexOf("onec_security", StringComparison.Ordinal);
                string onec = onecSecurity.Substring(pos);
                pos = onec.IndexOf(";", StringComparison.Ordinal);
                onecSecurity = onec.Substring(0, pos);

                webRequest1.UserAgent = "1C+Enterprise/8.3";
                webRequest1.KeepAlive = true;

                webRequest1 = (HttpWebRequest)WebRequest.Create(locationHeaderValue);
                webRequest1.Headers.Add("Cookie", "client_language=ru_RU; i18next=ru-RU; SESSION=" + jsessionid1 + "; " + onecSecurity);

                using (webRequest1.GetResponse())
                {

                }


                webRequest1 = (HttpWebRequest)WebRequest.Create(_productsTotalPage);
                webRequest1.UserAgent = "1C+Enterprise/8.3";
                webRequest1.KeepAlive = true;
                webRequest1.AllowAutoRedirect = false;
                webRequest1.Headers.Add("Cookie", jsessionid1 + "; " + onecSecurity);

                string jsessionid2;

                try
                {
                    using (var webResponse = (HttpWebResponse)webRequest1.GetResponse())
                    {
                        jsessionid2 = webResponse.Headers["Set-cookie"].Substring(0,
                        webResponse.Headers["Set-cookie"].IndexOf(";", StringComparison.Ordinal));
                        locationHeaderValue = webResponse.Headers["Location"];
                    }
                }
                catch (Exception)
                {
                    return false;
                }




                webRequest1 = (HttpWebRequest)WebRequest.Create(locationHeaderValue);
                webRequest1.UserAgent = "1C+Enterprise/8.3";
                webRequest1.KeepAlive = true;
                webRequest1.AllowAutoRedirect = false;
                webRequest1.Headers.Add("Cookie", jsessionid1 + "; " + onecSecurity);

                using (var webResponse = (HttpWebResponse)webRequest1.GetResponse())
                {
                    locationHeaderValue = webResponse.Headers["Location"];
                }


                webRequest1 = (HttpWebRequest)WebRequest.Create(locationHeaderValue);
                webRequest1.UserAgent = "1C+Enterprise/8.3";
                webRequest1.KeepAlive = true;
                webRequest1.Headers.Add("Cookie", jsessionid2);
                webRequest1.AllowAutoRedirect = false;

                using (webRequest1.GetResponse()) { }

                _jsoncode = jsessionid2;

                return IsUrlHasRespond(_productsTotalPage) && ContentLength != 0;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public override string GetProductsTotalPage()
        {
            return DownloadString(_productsTotalPage);
        }

        protected override HttpWebRequest BuildWebReqest(Uri link)
        {
            var webRequest1 = (HttpWebRequest)WebRequest.Create(link);
            //   webRequest1.UserAgent = "1C+Enterprise/8.3";
            webRequest1.KeepAlive = true;
            webRequest1.Headers.Add("Cookie", _jsoncode);
            webRequest1.AllowAutoRedirect = false;

            return webRequest1;
        }

        private string GetInviteCode(string str)
        {
            string inviteCode = "inviteCode=";

            var document = new HtmlDocument();

            document.LoadHtml(str);

            HtmlNodeCollection nodes = document.DocumentNode.SelectNodes("//input");

            foreach (HtmlNode node in nodes)
            {
                foreach (HtmlAttribute atr in node.Attributes)
                {
                    if (atr.Value == "lt")
                    {
                        inviteCode += "&lt=" + node.Attributes[2].Value;
                    }
                    if (atr.Value == "execution")
                    {
                        inviteCode += "&execution=" + node.Attributes[2].Value;
                    }
                    if (atr.Value == "_eventId")
                    {
                        inviteCode += "&_eventId=" + node.Attributes[2].Value;
                        //            found = true;
                    }
                }
            }

            return inviteCode;
        }

        public Uri GetProductUriByProductCode(string productCode)
        {
            return new Uri(AuthDataSource.BaseUri, string.Format("/project/{0}", productCode));
        }

        public IEnumerable<Uri> GetProductUri(string[] productCodes)
        {
            var uries = new Uri[productCodes.Length];

            for (int i = 0; i < productCodes.Length; i++)
            {
                uries[i] = GetProductUriByProductCode(productCodes[i]);
            }

            return uries;
        }

        public Uri GetVersionContentUri(string productCode, string versionNumber)
        {
            return new Uri(AuthDataSource.BaseUri, string.Format("/version_files?nick={0}&ver={1}", productCode, versionNumber));
        }

      public void SetAuthorizationDataSource(AuthDataSourceBase v8AuthData)
        {
            AuthDataSource = v8AuthData;
            _productsTotalPage = v8AuthData.ProductsTotalPage;

        }
    }
}