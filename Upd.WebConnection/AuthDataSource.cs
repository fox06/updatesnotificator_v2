﻿using System;
using Upd.WebConnection.Contracts;

namespace Upd.WebConnection
{
    public class AuthDataSource : AuthDataSourceBase
    {
        public AuthDataSource(string login, string password, Uri loginUri, Uri baseUri, Uri productsTotalPage) 
            : base(login, password, loginUri, baseUri, productsTotalPage)
        {

        }
    }
}