﻿using Autofac;
using Core.Common;
using Core.Common.Contracts;
using Upd.WebConnection.Contracts;

namespace Upd.WebConnection
{
    public class ServerConnectionFactory : IServerConnectionsFactory
    {
        ContainerBuilder autofac;
        private IContainer container;

        public ServerConnectionFactory()
        {
            autofac = new ContainerBuilder();

            autofac.RegisterType<ServerConnectionV8>().As<IServerConnectionV8>();
            autofac.RegisterType<ServerConnectionV7>().As<IServerConnectionV7>();
            
            container = autofac.Build();
        }
        T IServerConnectionsFactory.GetServerConnection<T>()
        {
            return container.Resolve<T>();
        }
    }
}
