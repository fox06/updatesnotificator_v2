﻿using System.Threading.Tasks;
using System.Windows;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace Upd.UserInterfaces.WPF.UtilServices
{
    public static class DialogService
    {
        public static Task<MessageDialogResult> ShowMessage(string title,
            string message, MessageDialogStyle dialogStyle)
        {
            var metroWindow = (Application.Current.MainWindow as MetroWindow);
            if (metroWindow != null)
            {
                metroWindow.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;
                return metroWindow.ShowMessageAsync(
                    title, message, dialogStyle, metroWindow.MetroDialogOptions);
            }

            return null;
        }
    }
}
