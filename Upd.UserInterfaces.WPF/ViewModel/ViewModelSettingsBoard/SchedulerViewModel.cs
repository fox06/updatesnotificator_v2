﻿using System.Collections.Generic;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Upd.Business;
using Upd.Business.Settings;

namespace Upd.UserInterfaces.WPF.ViewModel.ViewModelSettingsBoard
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class SchedulerViewModel : ViewModelBase
    {
        private readonly IAppSettingsManager _settingsService;
  
        public const string DaysName = "Days";

        public DaysOfWeek Days
        {
            get
            {
                return _settingsService.CurrentSettings.Scheduler.DaysOfWeeks;
            }

            set
            {
                if (_settingsService.CurrentSettings.Scheduler.DaysOfWeeks == value)
                {
                    return;
                }

                RaisePropertyChanging(DaysName);
                _settingsService.CurrentSettings.Scheduler.DaysOfWeeks = value;
                RaisePropertyChanged(DaysName);
            }
        }
       

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public SchedulerViewModel(IAppSettingsManager settingsService)
        {
            _settingsService = settingsService;
        }
    }
}