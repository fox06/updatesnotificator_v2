using System.Collections.Generic;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Upd.Business;
using Upd.Business.Settings;

namespace Upd.UserInterfaces.WPF.ViewModel.ViewModelSettingsBoard
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class SettingsDataBoardViewModel : ViewModelBase
    {
        private readonly IAppSettingsManager _settingsService;

        public const string V8LoginName = "V8Login";
        public const string V8PasswordName = "V8Password";

        public const string UseV8ProductsCheckName = "UseV8ProductsCheck";
        public const string UseV7ProductsCheckName = "UseV7ProductsCheck";

        public const string V7LoginName = "V7Login";
        public const string V7PasswordName = "V7Password";

        public const string DaysName = "DaysOfWeek";

        public DaysOfWeek DaysOfWeek
        {
            get
            {
                return _settingsService.CurrentSettings.Scheduler.DaysOfWeeks;
            }

            set
            {
                if (_settingsService.CurrentSettings.Scheduler.DaysOfWeeks == value)
                {
                    return;
                }

                RaisePropertyChanging(DaysName);
                _settingsService.CurrentSettings.Scheduler.DaysOfWeeks = value;
                RaisePropertyChanged(DaysName);
            }
        }
       
     
        private readonly RelayCommand _saveSettingsCommand;
        private readonly RelayCommand _loadSettingsCommand;

        public string V8Login
        {
            get
            {
                return _settingsService.CurrentSettings.V8AuthorizationSettings.Login;
            }

            set
            {
                if (_settingsService.CurrentSettings.V8AuthorizationSettings.Login == value)
                {
                    return;
                }

                RaisePropertyChanging(V8LoginName);
                _settingsService.CurrentSettings.V8AuthorizationSettings.Login = value;
                RaisePropertyChanged(V8LoginName);
            }
        }

        public string V8Password
        {
            get
            {
                return _settingsService.CurrentSettings.V8AuthorizationSettings.Password;
            }

            set
            {
                if (_settingsService.CurrentSettings.V8AuthorizationSettings.Password == value)
                {
                    return;
                }

                RaisePropertyChanging(V8PasswordName);
                _settingsService.CurrentSettings.V8AuthorizationSettings.Password = value;
                RaisePropertyChanged(V8PasswordName);
            }
        }

        public string V7Login
        {
            get
            {
                return _settingsService.CurrentSettings.V7AuthorizationSettings.Login;
            }

            set
            {
                if (_settingsService.CurrentSettings.V7AuthorizationSettings.Login == value)
                {
                    return;
                }

                RaisePropertyChanging(V7LoginName);
                _settingsService.CurrentSettings.V7AuthorizationSettings.Login = value;
                RaisePropertyChanged(V7LoginName);
            }
        }

        public string V7Password
        {
            get
            {
                return _settingsService.CurrentSettings.V7AuthorizationSettings.Password;
            }

            set
            {
                if (_settingsService.CurrentSettings.V7AuthorizationSettings.Password == value)
                {
                    return;
                }

                RaisePropertyChanging(V7PasswordName);
                _settingsService.CurrentSettings.V7AuthorizationSettings.Password = value;
                RaisePropertyChanged(V7PasswordName);
            }
        }


        public bool UseV8ProductsCheck
        {
            get
            {
                return _settingsService.CurrentSettings.UseV8ProductsCheck;
            }

            set
            {
                if (_settingsService.CurrentSettings.UseV8ProductsCheck == value)
                {
                    return;
                }

                RaisePropertyChanging(UseV8ProductsCheckName);
                _settingsService.CurrentSettings.UseV8ProductsCheck = value;
                RaisePropertyChanged(UseV8ProductsCheckName);
            }
        }

        public bool UseV7ProductsCheck
        {
            get
            {
                return _settingsService.CurrentSettings.UseV7ProductsCheck;
            }

            set
            {
                if (_settingsService.CurrentSettings.UseV7ProductsCheck == value)
                {
                    return;
                }

                RaisePropertyChanging(UseV7ProductsCheckName);
                _settingsService.CurrentSettings.UseV7ProductsCheck = value;
                RaisePropertyChanged(UseV7ProductsCheckName);
            }
        }

        public RelayCommand SaveSettingsCommand
        {
            get
            {
                return _saveSettingsCommand;
            }
        }

        public RelayCommand LoadSettingsCommand
        {
            get
            {
                return _loadSettingsCommand;
            }
        }
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public SettingsDataBoardViewModel(IAppSettingsManager settingsService)
        {
            settingsService.LoadSettings();
            _settingsService = settingsService;
            
       
            _saveSettingsCommand = new RelayCommand(SaveSettings);
            _loadSettingsCommand = new RelayCommand(LoadSettings);
        }

        public void LoadSettings()
        {
            _settingsService.LoadSettings();
        }

        private void SaveSettings()
        {
            _settingsService.SaveSettings();
        }
    }
}