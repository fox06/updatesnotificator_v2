﻿using System.Threading.Tasks;
using Core.Common.Contracts;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Threading;
using MahApps.Metro.Controls.Dialogs;
using Upd.Business;
using Upd.Data.Contracts;
using Upd.UserInterfaces.WPF.UtilServices;
using Upd.UserInterfaces.WPF.ViewModel.ViewModelActualDataBoard;

namespace Upd.UserInterfaces.WPF.ViewModel.Commands
{
    public class MainServiceCommands : ViewModelBase
    {
        private readonly IUpdNotifierEngine _dataService;

        private readonly RelayCommand _startDataReciving;
        private readonly RelayCommand _cancelDataReciving;

        public MainServiceCommands(IUpdNotifierEngine dataService, IDataRepositoryFactory repositoryFactory)
        {
            _dataService = dataService;
            _historyBoard = SimpleIoc.Default.GetInstance<ActualProductsDataBoardViewModel>();
           
            _startDataReciving = new RelayCommand(Begin);
            _cancelDataReciving = new RelayCommand(() => _dataService.Cancel());
        }

        public RelayCommand StartCommand
        {
            get
            {
                return _startDataReciving;
            }
        }

        public RelayCommand CancelCommand
        {
            get
            {
                return _cancelDataReciving;
            }
        }


        public const string IsBusyPropertyName = "IsBusy";

        private bool _isBusy;
        private readonly ActualProductsDataBoardViewModel _historyBoard;


        public bool IsBusy
        {
            get
            {
                return _isBusy;
            }

            set
            {
                if (_isBusy == value)
                {
                    return;
                }

                RaisePropertyChanging(IsBusyPropertyName);
                _isBusy = value;
                RaisePropertyChanged(IsBusyPropertyName);
            }
        }


        private void Begin()
        {
            if (IsBusy)
            {
                CancelCommand.Execute(false);
                return;
            }

            //IsBusy = true;

            if (_dataService.IsDbEmpty())
            {
                 DialogService.ShowMessage("", "Загрузить данные о доступных конфигурациях?",
                    MessageDialogStyle.AffirmativeAndNegative).ContinueWith((r) =>
                    {
                        IsBusy = true;

                        if (r.Result == MessageDialogResult.Affirmative)
                            _dataService.GetProducts();

                        IsBusy = false;


                    }).ContinueWith(obj => DispatcherHelper.CheckBeginInvokeOnUI(() =>
                    {
                        _historyBoard.LoadData();

                        IsBusy = false;
                    }));
            }
            else
            {
               Task.Factory.StartNew(() =>
               {
                   IsBusy = true;
                   _dataService.Start();
               }).ContinueWith(
                    obj => DispatcherHelper.CheckBeginInvokeOnUI(() =>
                    {
                        _historyBoard.LoadData();

                        IsBusy = false;
                    }));
            }


           

        }

       
      
    }


}
