/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:Upd.ui.wpf"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using Core.Common.Contracts;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using Upd.Business;
using Upd.Business.Contracts;
using Upd.Business.Managers;
using Upd.Business.Settings;
using Upd.Data;
using Upd.Parser;
using Upd.UserInterfaces.WPF.Mock;
using Upd.UserInterfaces.WPF.ViewModel.Commands;
using Upd.UserInterfaces.WPF.ViewModel.ViewModelActualDataBoard;
using Upd.UserInterfaces.WPF.ViewModel.ViewModelSettingsBoard;
using Upd.WebConnection;

namespace Upd.UserInterfaces.WPF.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            bool debug = true;

            if (ViewModelBase.IsInDesignModeStatic || debug)
            {
                if (!SimpleIoc.Default.IsRegistered<IDataRepositoryFactory>())
                    SimpleIoc.Default.Register<IDataRepositoryFactory, MockDataRepositoryFactory>();

                SimpleIoc.Default.Register<IUpdNotifierEngine, UpdEngine>();
                SimpleIoc.Default.Register<IProductsChangesProcessor, ProductsChangesProcessor>();
                SimpleIoc.Default.Register<IAppSettingsManager, AppSettingsManager>();
                SimpleIoc.Default.Register<IServerDataManager, ServerDataManager>();
                SimpleIoc.Default.Register<IServerConnectionsFactory, ServerConnectionFactory>();
                SimpleIoc.Default.Register<IDataParsersFactory, DataParsersFactory>();
                
                //if (!SimpleIoc.Default.IsRegistered<IProcessorService>())
                //    SimpleIoc.Default.Register<IProcessorService, ProcessorServiceDesign>();

             //   if (!SimpleIoc.Default.IsRegistered<IAppSettingsManager>())
               //     SimpleIoc.Default.Register<IAppSettingsManager, SettingsServiceDesign>();
                
               
             //   SimpleIoc.Default.Register<IProcessorService, ProcessorService>();
            //    SimpleIoc.Default.Register<IHistoryChangesService, HistoryChangesService>();
            //    SimpleIoc.Default.Register<IProductService, ProductService>();
            //    SimpleIoc.Default.Register<IContentService, ContentService>();
           //     SimpleIoc.Default.Register<IAppSettingsService, AppSettingsServiceMOCK>();
            //    SimpleIoc.Default.Register<IDownloaderService, DownloaderService>();
            }
            else
            {
                SimpleIoc.Default.Register<IDataRepositoryFactory, DataRepositoryFactory>();
                SimpleIoc.Default.Register<IUpdNotifierEngine, UpdEngine>();
                SimpleIoc.Default.Register<IProductsChangesProcessor, ProductsChangesProcessor>();
                SimpleIoc.Default.Register<IAppSettingsManager, AppSettingsManager>();
                SimpleIoc.Default.Register<IServerDataManager, ServerDataManager>();
                SimpleIoc.Default.Register<IServerConnectionsFactory, ServerConnectionFactory>();
                SimpleIoc.Default.Register<IDataParsersFactory, DataParsersFactory>();
             //   SimpleIoc.Default.Register<IProcessorService, ProcessorService>();
             //   SimpleIoc.Default.Register<IAppSettingsManager, SettingsService>();
                
                //SimpleIoc.Default.Register<IProcessorService, ProcessorService>();
                //SimpleIoc.Default.Register<IHistoryChangesService, HistoryChangesService>();
                //SimpleIoc.Default.Register<IProductService, ProductService>();
                //SimpleIoc.Default.Register<IContentService, ContentService>();
                //SimpleIoc.Default.Register<IAppSettingsService, AppSettingsService>();
                //SimpleIoc.Default.Register<IDownloaderService, DownloaderService>();
            }



            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<ActualProductsDataBoardViewModel>();
            SimpleIoc.Default.Register<SettingsDataBoardViewModel>();

            //SimpleIoc.Default.Register<ProductsBoardViewModel>();
            //SimpleIoc.Default.Register<ViewDownloadManager>();
            //SimpleIoc.Default.Register<HistoriesRecordsViewModel>();
            //SimpleIoc.Default.Register<ProductsRecordsViewModel>();
            SimpleIoc.Default.Register<MainServiceCommands>();
            //SimpleIoc.Default.Register<SettingProfilesViewModel>();
        }

        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }

        public ActualProductsDataBoardViewModel ActualProductsDataBoard
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ActualProductsDataBoardViewModel>();
            }
        }


        public SettingsDataBoardViewModel SettingsDataBoard
        {
            get
            {
                return ServiceLocator.Current.GetInstance<SettingsDataBoardViewModel>();
            }
        }


        //public ProductsBoardViewModel ProductsBoard
        //{
        //    get
        //    {
        //        return ServiceLocator.Current.GetInstance<ProductsBoardViewModel>();
        //    }
        //}



        public MainServiceCommands MainServiceCommands
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainServiceCommands>();
            }
        }

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }

    
}