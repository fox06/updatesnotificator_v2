using System;
using System.Windows.Media.Imaging;
using Upd.Business;
using Upd.Business.Entities;
using Version = Upd.Business.Entities.Version;

namespace Upd.UserInterfaces.WPF.ViewModel.ViewModelActualDataBoard
{
    public class ProductVersionViewModel
    {
        private static readonly BitmapImage NewBitmapImage = new BitmapImage(new Uri(@"label_new.png", UriKind.Relative));

        private readonly Version _productVersion;
        private readonly string _versionDiscription;

        private readonly BitmapImage _eventImage;

        public string VersionNumber { get { return _productVersion.VersionNumber; } }

        public string VersionDiscription
        {
            get
            {
                return _versionDiscription;
            }
        }

        public BitmapImage EventImage
        {
            get { return _eventImage; }
        }

        public ProductVersionViewModel(Version productVersion, VersionChange versionChange = null)
        {
           _productVersion = productVersion;

            if (productVersion.Date != null)
                _versionDiscription = productVersion.VersionNumber + " (" +
                                      productVersion.Date.Value.ToShortDateString() + ")";

            if (versionChange != null && (versionChange.ChangeEvent == VersionChangesEvents.Added || versionChange.ChangeEvent == VersionChangesEvents.DateChanged))
           {
               _eventImage = NewBitmapImage;
           }
         
        }
    }
}