﻿using System.Collections.ObjectModel;
using Upd.Business.Entities;
using Upd.Data.Contracts.DTO;

namespace Upd.UserInterfaces.WPF.ViewModel.ViewModelActualDataBoard
{
    public class ActualDataViewModel
    {
        private readonly ProductDTO _product;
        private readonly ObservableCollection<ProductVersionViewModel> _plannedVersionsCollection;
        private readonly ObservableCollection<ProductVersionViewModel> _testVersionsCollection;
        private readonly ObservableCollection<ProductVersionViewModel> _releasedVersionsCollection;


        public string ProductName { get { return _product.Name; } }

        public ObservableCollection<ProductVersionViewModel> PlannedVersionsCollection { get { return _plannedVersionsCollection; } }
        public ObservableCollection<ProductVersionViewModel> TestVersionsCollection { get { return _testVersionsCollection; } }
        public ObservableCollection<ProductVersionViewModel> ReleasedVersionsCollection { get { return _releasedVersionsCollection; } }

        public ActualDataViewModel(ProductDTO product)
        {
            _product = product;

            _plannedVersionsCollection = new ObservableCollection<ProductVersionViewModel>();
            _releasedVersionsCollection = new ObservableCollection<ProductVersionViewModel>();
            _testVersionsCollection = new ObservableCollection<ProductVersionViewModel>();

            foreach (var version in _product.Versions)
            {
                if(version.VersionType == VersionTypes.Planned)
                    _plannedVersionsCollection.Add(new ProductVersionViewModel(version));

                if (version.VersionType == VersionTypes.Preview)
                    _testVersionsCollection.Add(new ProductVersionViewModel(version));

                if (version.VersionType == VersionTypes.Release)
                    _releasedVersionsCollection.Add(new ProductVersionViewModel(version));
            }
        }

    }
}