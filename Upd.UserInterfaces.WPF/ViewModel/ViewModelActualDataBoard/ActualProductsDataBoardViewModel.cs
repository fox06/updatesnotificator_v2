using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Upd.Business;

namespace Upd.UserInterfaces.WPF.ViewModel.ViewModelActualDataBoard
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class ActualProductsDataBoardViewModel : ViewModelBase
    {
        private readonly IUpdNotifierEngine _actualProductDataService;

        private RelayCommand _selectedHistoryRecordChanged;
        private ObservableCollection<ActualDataViewModel> _actualProductsDataCollection;
        private ActualDataViewModel _selectedRecord;

        public const string SelectedRecordPropertyName = "SelectedRecord";
        public const string ActualProductsDataPropertyName = "ActualProductsData";

        public ObservableCollection<ActualDataViewModel> ActualProductsDataCollection
        {
            get
            {
                return _actualProductsDataCollection;
            }

            set
            {
                if (_actualProductsDataCollection == value)
                {
                    return;
                }

                RaisePropertyChanging(ActualProductsDataPropertyName);
                _actualProductsDataCollection = value;
                RaisePropertyChanged(ActualProductsDataPropertyName);
            }
        }

       
        public ActualDataViewModel SelectedRecord
        {
            get
            {
                return _selectedRecord;
            }

            set
            {
                if (_selectedRecord == value)
                {
                    return;
                }

                RaisePropertyChanging(SelectedRecordPropertyName);
                _selectedRecord = value;
                SelectedHistoryRecordChanged.Execute(null);
                RaisePropertyChanged(SelectedRecordPropertyName);
            }
        }
      

        public RelayCommand SelectedHistoryRecordChanged
        {
            get
            {

                return _selectedHistoryRecordChanged
                      ?? (_selectedHistoryRecordChanged = new RelayCommand(
                          () =>
                          {
                              if (SelectedRecord == null)
                              {
                                  return;
                              }
                              //LoadContentByObject(SelectedRecord);


                          }));
            }
        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public ActualProductsDataBoardViewModel(IUpdNotifierEngine productsManager)
        {
            _actualProductDataService = productsManager;
            LoadData();

        }

        public void LoadData()
        {
            if (_actualProductsDataCollection == null)
                _actualProductsDataCollection = new ObservableCollection<ActualDataViewModel>();
            else
                _actualProductsDataCollection.Clear();


            foreach (var item in _actualProductDataService.GetAllProductsWithVersions())
            {
                var advm = new ActualDataViewModel(item);
                _actualProductsDataCollection.Add(advm);
            }
        }

      
    }
}