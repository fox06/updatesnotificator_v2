using System;
using GalaSoft.MvvmLight;
using Upd.Business;
using Upd.Business.ProgressReporter;

namespace Upd.UserInterfaces.WPF.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        public const string ProcessProgressPropertyName = "ProcessProgress";
        public const string ProcessProgressInfoMsgPropertyName = "ProcessProgressInfoMsg";
        

        private int _processProgress;
        private string _processProgressInfoMsgs;
        private IDisposable _messagerUnsubscriber;


        public int ProcessProgress
        {
            get
            {
                return _processProgress;
            }

            set
            {
                if (_processProgress == value)
                {
                    return;
                }

                RaisePropertyChanging(ProcessProgressPropertyName);
                _processProgress = value;
                RaisePropertyChanged(ProcessProgressPropertyName);
            }
        }

        public string ProcessProgressInfoMsg
        {
            get
            {
                return _processProgressInfoMsgs;
            }

            set
            {
                if (_processProgressInfoMsgs == value)
                {
                    return;
                }

                RaisePropertyChanging(ProcessProgressInfoMsgPropertyName);
                _processProgressInfoMsgs = value;
                RaisePropertyChanged(ProcessProgressInfoMsgPropertyName);
            }
        }
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IUpdNotifierEngine processorService)
        {
            var not = new NotificationListener(ReciveMessage);
            _messagerUnsubscriber = processorService.ProcessNotificatorText.Subscribe(not);

            _processProgress = 0;
            _processProgressInfoMsgs = "";
        }

        private void ReciveMessage(IProcessNotificationMsg notification)
        {
            ProcessProgressInfoMsg = notification.Notification;
            ProcessProgress = notification.ProgressPercentage;
        }

        public override void Cleanup()
        {
            _messagerUnsubscriber.Dispose();
            base.Cleanup();
        }
    }
}