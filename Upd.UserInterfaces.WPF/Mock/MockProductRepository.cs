using System;
using System.Collections.Generic;
using System.Linq;
using Upd.Business.Entities;
using Upd.Data.Contracts;
using Upd.Data.Contracts.DTO;
using Version = Upd.Business.Entities.Version;

namespace Upd.UserInterfaces.WPF.Mock
{
    public class MockProductRepository: IProductsRepository
    {
        List<Product> productList = new List<Product>();

        public MockProductRepository()
        {
            productList.Clear();

            var p1 = new Product { CheckForUpdates = true, Name = "�������� � ����������� ���������� 3.0", ProductCode = "HRM" };
            var v1 = new Version { ProductId = p1.ProductId, VersionNumber = "1.4.45", Date = DateTime.Now, VersionType = VersionTypes.Release};
            var v2 = new Version { ProductId = p1.ProductId, VersionNumber = "2.6", Date = DateTime.Now, VersionType = VersionTypes.Planned};
            var v3 = new Version { ProductId = p1.ProductId, VersionNumber = "2.7", Date = DateTime.Now, VersionType = VersionTypes.Planned };
            
            p1.AddVersion(v1);
            p1.AddVersion(v2);
            p1.AddVersion(v3);

            productList.Add(p1);

            p1= new Product { CheckForUpdates = true, Name = "����������� ���������������� ����������(�������� ������) 4.0", ProductCode = "BGU" };
            v1 = new Version { ProductId = p1.ProductId, VersionNumber = "4.1", Date = DateTime.Now, VersionType = VersionTypes.Release };
            v2 = new Version { ProductId = p1.ProductId, VersionNumber = "4.5", Date = DateTime.Now, VersionType = VersionTypes.Planned };
            v3 = new Version { ProductId = p1.ProductId, VersionNumber = "5.0", Date = DateTime.Now, VersionType = VersionTypes.Preview };

            p1.AddVersion(v1);
            p1.AddVersion(v2);
            p1.AddVersion(v3);

            productList.Add(p1);
        }

        public Product Add(Product entity)
        {
            productList.Add(entity);

            return entity;
        }

        public void InsertOrUpdate(IEnumerable<Product> entities)
        {
            throw new NotImplementedException();
        }


        public void Remove(Product entity)
        {
            throw new NotImplementedException();
        }

        public void Remove(Guid id)
        {
            throw new NotImplementedException();
        }

        public Product Update(Product entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Product> Get()
        {
            return productList;
        }

        public Product Get(Guid id)
        {
            throw new NotImplementedException();
        }

        public void InsertEntities(IEnumerable<Product> entities)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Product> GetAllActualProductDatas()
        {
           
            return productList;

        }

        public int Count()
        {
            return productList.Count();
        }

        public Product GetByProductCode(string productCode)
        {
            foreach (var product in productList)
            {
                if (product.ProductCode == productCode)
                    return product;

            }

            return null;
        }

        public IEnumerable<ProductDTO> GetAllProductsWithVersions()
        {
            var productsDTO = new List<ProductDTO>();

            foreach (var product in productList)
            {
                var dto = new ProductDTO
                {
                    ProductCode = product.ProductCode,
                    Name = product.Name,
                    EntityId = product.EntityId,
                    Platform = product.Platform,
                    Versions = product.Versions
                };
           

                productsDTO.Add(dto);
            }

            return productsDTO;
        }
    }
}