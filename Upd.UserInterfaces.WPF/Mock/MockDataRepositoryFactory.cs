using System.Collections;
using Core.Common.Contracts;

namespace Upd.UserInterfaces.WPF.Mock
{
    public class MockDataRepositoryFactory : IDataRepositoryFactory
    {
        private readonly Hashtable _hash = new Hashtable();
        private MockProductRepository pr;
        private MockVersionRepository vs;

        public MockDataRepositoryFactory()
        {
            //var pr = new MockProductRepository();
            //var hs = new MockHistoryRepository();
            //var vs = new MockVersionRepository();
            //var ct = new MockContentRepository();

            pr = new MockProductRepository();
            //  var hs = new ActualDataRepository();
            vs = new MockVersionRepository();

            _hash.Add("IProductsRepository", pr);
            //   _hash.Add("IActualProductDataRepository", hs);
            _hash.Add("IProductVersionRepository", vs);

        }

  

       

        T IDataRepositoryFactory.GetDataRepository<T>()
        {
            return (T) _hash[typeof (T).Name];
        }
    }
}