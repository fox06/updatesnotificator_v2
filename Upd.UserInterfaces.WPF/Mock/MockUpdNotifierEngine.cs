﻿using System;
using System.Collections.Generic;
using Upd.Business;
using Upd.Business.Entities;
using Upd.Business.ProgressReporter;
using Upd.Data.Contracts.DTO;

namespace Upd.UserInterfaces.WPF.Mock
{
    public class MockUpdNotifierEngine : IUpdNotifierEngine
    {
        public void Cancel()
        {
            throw new NotImplementedException();
        }

        public void Start()
        {
            throw new NotImplementedException();
        }

        public void GetProducts()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Product> GetAllProducts()
        {
            throw new NotImplementedException();
        }

        public bool IsDbEmpty()
        {
            throw new NotImplementedException();
        }

        public IProcessNotificator ProcessNotificatorText { get; set; }
        public IEnumerable<ProductDTO> GetAllProductsWithVersions()
        {
            throw new NotImplementedException();
        }
    }
}