﻿using System;
using System.Collections.Generic;
using Core.Common.Contracts;
using Upd.Business.Entities;
using Upd.Data.Contracts;
using Version = Upd.Business.Entities.Version;

namespace Upd.UserInterfaces.WPF.Mock
{
    public class MockVersionRepository : IVersionsRepository
    {
        
        public Version Add(Version entity)
        {
            throw new NotImplementedException();
        }

        public void InsertOrUpdate(IEnumerable<Version> entities)
        {
            throw new NotImplementedException();
        }


        public void Remove(Version entity)
        {
            throw new NotImplementedException();
        }

        public void Remove(Guid id)
        {
            throw new NotImplementedException();
        }

        public Version Update(Version entity)
        {
            throw new NotImplementedException();
        }

        IEnumerable<Version> IDataRepository<Version>.Get()
        {
            throw new NotImplementedException();
        }

        Version IDataRepository<Version>.Get(Guid id)
        {
            throw new NotImplementedException();
        }


        public void RemoveByProductId(Guid productId)
        {
            //for (int i = versions.Count - 1; i >= 0; i--)
            //{
            //    if (versions[i].Id == productId)
            //        versions.RemoveAt(i);
            //}
        }


        public IEnumerable<Version> GetOnlyActualVersionsByProductId(Guid productId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Version> GetOnlyActualVersionsByProductId(Guid productId, VersionTypes versionType)
        {
            throw new NotImplementedException();
        }
    }
}
