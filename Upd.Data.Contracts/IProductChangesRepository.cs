﻿using System.Collections;
using System.Linq;
using System.Text;
using Core.Common.Contracts;
using Upd.Business.Entities;

namespace Upd.Data.Contracts
{
    public interface IProductChangesRepository : IDataRepository<ProductChange>
    {

    }
}


