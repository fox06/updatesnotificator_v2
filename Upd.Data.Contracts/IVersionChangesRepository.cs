﻿using Core.Common.Contracts;
using Upd.Business.Entities;

namespace Upd.Data.Contracts
{
    public interface IVersionChangesRepository : IDataRepository<VersionChange>
    {
        
    }
}