﻿using System;
using System.Collections.Generic;
using Upd.Business.Entities;
using Version = Upd.Business.Entities.Version;

namespace Upd.Data.Contracts.DTO
{
    // ReSharper disable once InconsistentNaming
    public class ProductDTO
    {
        public Guid EntityId { get; set; }
        public string ProductCode { get; set; }
        public string Name { get; set; }
        public Platform Platform { get; set; }

        public IList<Version> Versions { get; set; }

        public ProductDTO()
        {
            Versions = new List<Version>();
        }
    }

 }

