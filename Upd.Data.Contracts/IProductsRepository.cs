using System.Collections.Generic;
using Core.Common.Contracts;
using Upd.Business.Entities;
using Upd.Data.Contracts.DTO;

namespace Upd.Data.Contracts
{
    public interface IProductsRepository : IDataRepository<Product>
    {
        Product GetByProductCode(string productCode);
        IEnumerable<ProductDTO> GetAllProductsWithVersions();
    }
}