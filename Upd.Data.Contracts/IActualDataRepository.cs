﻿using Core.Common.Contracts;
using Upd.Business.Entities;

namespace Upd.Data.Contracts
{
    public interface IActualDataRepository : IDataRepository<ActualDataRecord>
    {

    }
}