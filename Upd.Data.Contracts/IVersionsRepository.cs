﻿using System;
using System.Collections.Generic;
using Core.Common.Contracts;
using Upd.Business.Entities;
using Version = Upd.Business.Entities.Version;

namespace Upd.Data.Contracts
{
    public interface IVersionsRepository : IDataRepository<Version>
    {
        IEnumerable<Version> GetOnlyActualVersionsByProductId(Guid productId);
        IEnumerable<Version> GetOnlyActualVersionsByProductId(Guid productId, VersionTypes versionType);
    }
}