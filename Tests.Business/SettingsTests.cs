﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Upd.Business.Managers;

namespace Tests.Business
{
    [TestFixture]
    public class SettingsTests
    {
        [Test]
        public void SaveLoadTest()
        {
            AppSettingsManager manager = new AppSettingsManager();
            manager.CurrentSettings.V8AuthorizationSettings.Login = "test_login";

            manager.SaveSettings();

            manager = new AppSettingsManager();
            manager.LoadSettings();

            Assert.AreEqual(manager.CurrentSettings.V8AuthorizationSettings.Login, "test_login"); 
        }
    }
}
