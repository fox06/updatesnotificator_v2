﻿using System;

namespace Upd.WebConnection.Contracts
{
    public abstract class AuthDataSourceBase
    {
        public string Password { get; private set; }
        public Uri LoginUri { get; set; }

        public string Login { get; private set; }
        public Uri BaseUri { get; set; }

        public Uri ProductsTotalPage { get; private set; }

        public AuthDataSourceBase(string login, string password, Uri loginUri, Uri baseUri, Uri productsTotalPage)
        {
            ProductsTotalPage = productsTotalPage;
            Password = password;
            LoginUri = loginUri;
            BaseUri = baseUri;
            Login = login;
        }
    }
}