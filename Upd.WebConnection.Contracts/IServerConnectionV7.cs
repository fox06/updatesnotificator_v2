﻿using System;
using Core.Common.Contracts;

namespace Upd.WebConnection.Contracts
{
    public interface IServerConnectionV7 : IServerConnection
    {
        Uri GetProductsUri();
        Uri GetReportsUri();
        void SetAuthorizationDataSource(AuthDataSourceBase authDataSource);
    }
}