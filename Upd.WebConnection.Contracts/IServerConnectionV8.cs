﻿using System;
using System.Collections.Generic;
using Core.Common.Contracts;

namespace Upd.WebConnection.Contracts
{
    public interface IServerConnectionV8 : IServerConnection
    {
        Uri GetProductUriByProductCode(string productCode);
        IEnumerable<Uri> GetProductUri(string[] productCodes);
        Uri GetVersionContentUri(string productCode, string versionNumber);

        void SetAuthorizationDataSource(AuthDataSourceBase authData);

    }
}
