using System.Collections.Generic;
using Core.Common.Contracts;
using Upd.Business.Entities;
using Upd.Business.ProgressReporter;
using Upd.Data.Contracts.DTO;

namespace Upd.Business
{
    public interface IUpdNotifierEngine : IAppEngine
    {
        void Cancel();
        void Start();
        void GetProducts();
        IEnumerable<Product> GetAllProducts();
        bool IsDbEmpty();
        IProcessNotificator ProcessNotificatorText { get; set; }
        IEnumerable<ProductDTO> GetAllProductsWithVersions();
    }
}