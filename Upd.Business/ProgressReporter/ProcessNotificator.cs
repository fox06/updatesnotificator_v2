﻿using System;
using System.Collections.Generic;

namespace Upd.Business.ProgressReporter
{
    public class ProcessNotificator : IObservable<IProcessNotificationMsg>, IProcessNotificator
    {
        private readonly List<IObserver<IProcessNotificationMsg>> _observers;

        public ProcessNotificator()
        {
            _observers = new List<IObserver<IProcessNotificationMsg>>();
        }

        public IDisposable Subscribe(IObserver<IProcessNotificationMsg> observer)
        {
            if (!_observers.Contains(observer))
            {
                _observers.Add(observer);
                // Provide observer with existing data.
                //foreach (var item in flights)
                //    observer.OnNext(item);
            }

            return new Unsubscriber(_observers, observer);
        }

        public void SendNotification(IProcessNotificationMsg message)
        {
            foreach (var observer in _observers)
            {
                observer.OnNext(message);
            }
        }
    }
}
