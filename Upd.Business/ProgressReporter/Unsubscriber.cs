﻿using System;
using System.Collections.Generic;

namespace Upd.Business.ProgressReporter
{
    public class Unsubscriber : IDisposable
    {
        private List<IObserver<IProcessNotificationMsg>> _observers;
        private IObserver<IProcessNotificationMsg> _observer;

        internal Unsubscriber(List<IObserver<IProcessNotificationMsg>> observers, IObserver<IProcessNotificationMsg> observer)
        {
            this._observers = observers;
            this._observer = observer;
        }

        public void Dispose()
        {
            if (_observers.Contains(_observer))
                _observers.Remove(_observer);
        }
    }
}