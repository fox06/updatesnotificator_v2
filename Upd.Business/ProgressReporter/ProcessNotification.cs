﻿namespace Upd.Business.ProgressReporter
{
    public class ProcessNotification : IProcessNotificationMsg
    {
        public string Notification { get; private set; }
        public int ProgressPercentage { get; private set; }

        public ProcessNotification(string notification)
        {
            Notification = notification;
            ProgressPercentage = 0;
        }

        public ProcessNotification(string notification, int progressPercentage)
        {
            Notification = notification;
            ProgressPercentage = progressPercentage;
        }
    }
}