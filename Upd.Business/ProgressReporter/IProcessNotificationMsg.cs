﻿namespace Upd.Business.ProgressReporter
{
    public interface IProcessNotificationMsg
    {
        string Notification { get;  }
        int ProgressPercentage { get; }
    }
}