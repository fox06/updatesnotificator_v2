﻿using System;

namespace Upd.Business.ProgressReporter
{
    public interface IProcessNotificator
    {
        void SendNotification(IProcessNotificationMsg message);

        IDisposable Subscribe(IObserver<IProcessNotificationMsg> observer);
    }
}