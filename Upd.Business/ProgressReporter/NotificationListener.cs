﻿using System;

namespace Upd.Business.ProgressReporter
{
    public class NotificationListener : IObserver<IProcessNotificationMsg>
    {
        private readonly Action<IProcessNotificationMsg> _action;

        public NotificationListener(Action<IProcessNotificationMsg> action)
        {
            _action = action;
        }

        public void OnCompleted()
        {
            throw new NotImplementedException();
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnNext(IProcessNotificationMsg value)
        {
            _action.Invoke(value);
        }
    }
}
