namespace Upd.Business.Settings
{
    public class UpdatesCheckSheduler
    {
       // public bool[] DaysOfWeeks { get; set; }
        public DaysOfWeek DaysOfWeeks { get; set; }

        public int CheckPeriodBegin { get; set; }
        public int CheckPeriodEnd { get; set; }
        public int CheckPeriodMinutes { get; set; }

        public UpdatesCheckSheduler()
        {
            CheckPeriodBegin = 0;
            CheckPeriodEnd = 0;
            CheckPeriodMinutes = 60;
        }
    }
}