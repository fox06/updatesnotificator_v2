﻿using System;

namespace Upd.Business.Settings
{
    public class SettingsGroup
    {
        public AuthorizationSettings V8AuthorizationSettings { get; set; }
        public AuthorizationSettings V7AuthorizationSettings { get; set; }

        public UpdatesCheckSheduler Scheduler { get; set; }

        public bool UseV8ProductsCheck { get; set; }

        public bool UseV7ProductsCheck { get; set; }

        public bool UseAutodownloadUpdates { get; set; }
        public string DownloadPath { get; set; }

        public SettingsGroup()
        {
            UseV8ProductsCheck = false;
            UseV7ProductsCheck = false;

            UseAutodownloadUpdates = false;
            DownloadPath = string.Empty;

            Scheduler = new UpdatesCheckSheduler();

            V8AuthorizationSettings = new AuthorizationSettings();
            V7AuthorizationSettings = new AuthorizationSettings();
        }
    }
}