namespace Upd.Business.Settings
{
    public class AuthorizationSettings
    {
        public string Login { get; set; }
        public string Password { get; set; }

        public AuthorizationSettings()
        {
            Login = string.Empty;
            Password = string.Empty;
        }
    }
}
