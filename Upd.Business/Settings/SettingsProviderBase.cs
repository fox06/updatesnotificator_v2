﻿using System;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;

namespace Upd.Business.Settings
{
    public abstract class SettingsProviderBase<T> where T : class, new()
    {
        protected readonly string SettingsFilepath;
        
        protected SettingsProviderBase(string settingsFilepath)
        {
            SettingsFilepath = settingsFilepath;
        }
        
    
        protected void Save(T settings)
        {
            using (StreamWriter sw = new StreamWriter(SettingsFilepath))
            {
                XmlSerializer xmls = new XmlSerializer(typeof(T));
                xmls.Serialize(sw, settings);
            }
        }

        protected T Load()
        {
            using (var sw = new StreamReader(SettingsFilepath))
            {
                XmlSerializer xmls = new XmlSerializer(typeof(T));
                return xmls.Deserialize(sw) as T;
            }

           // return xmls.Deserialize(sw) as T;
        }

    }
}