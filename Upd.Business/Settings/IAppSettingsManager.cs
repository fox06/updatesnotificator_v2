﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;

namespace Upd.Business.Settings
{
    public interface IAppSettingsManager
    {
        SettingsGroup CurrentSettings { get; set; }

        void LoadSettings();
        void SaveSettings();
    }
}