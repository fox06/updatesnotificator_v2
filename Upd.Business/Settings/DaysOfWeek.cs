using System.Collections.Generic;
using Core.Common.Utils;

namespace Upd.Business.Settings
{
    public sealed class DaysOfWeek
    {
        private SerializableDictionary<string, bool> _days;

        public SerializableDictionary<string, bool> Days
        {
            get { return _days; }
        }

        public DaysOfWeek()
        {
            _days = new SerializableDictionary<string, bool>()
            {
                {"�����������", false},
                {"�������", false},
                {"�����", false},
                {"�������", false},
                {"�������", false},
                {"�������", false},
                {"�����������", false}
            };
            //this.A = new List<ShedulerRecord>
            //{
            //    new ShedulerRecord("�����������", false),
            //    new ShedulerRecord("�������", false),
            //    new ShedulerRecord("�����", false),
            //    new ShedulerRecord("�������", false),
            //    new ShedulerRecord("�������", false),
            //    new ShedulerRecord("�������", false),
            //    new ShedulerRecord("�����������", false)
            //};
        }

       
    }
}