using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Common;
using Core.Common.Contracts;
using Upd.Business.Contracts;
using Upd.Business.Entities;
using Upd.Business.ProgressReporter;
using Upd.Business.Settings;
using Upd.Data.Contracts;
using Upd.Data.Contracts.DTO;
using Upd.Parser.Contracts;
using Upd.WebConnection;
using Upd.WebConnection.Contracts;

namespace Upd.Business
{
    public class UpdEngine : EngineBase, IUpdNotifierEngine
    {
        private readonly IDataRepositoryFactory _dataRepositoryFactory;
        private readonly IServerConnectionsFactory _serverConnectionsFactory;
        private readonly IAppSettingsManager _appAppSettingsManager;
        private readonly IDataParsersFactory _dataParsersFactory;
        private readonly IProductsChangesProcessor _productsChangesProcessor;
        private readonly IServerDataManager _serverDataManager;
        private readonly IDownloader _downloader;
        private int serverConnectionAttempts = 5;

        Uri V8LoginPage = new Uri("https://login.1c.ru/login");
        Uri V7LoginPage = new Uri("http://techsupp.1c.ru/");

        Uri V8BaseUri = new Uri("https://releases.1c.ru/");
        Uri V7BaseUri = new Uri("http://techsupp.1c.ru/");

        Uri V8ProductsTotalPage = new Uri("https://releases.1c.ru/total/");
        Uri V7ProductsTotalPage = new Uri("http://techsupp.1c.ru/categ.jsp?GroupID=88");

        public UpdEngine(IDataRepositoryFactory dataRepositoryFactory,
            IProductsChangesProcessor productsChangesProcessor, IAppSettingsManager appSettingsManager,
            IServerDataManager serverDataManager, IServerConnectionsFactory serverConnectionsFactory,
            IDataParsersFactory dataParsersFactory)
        {
            _dataRepositoryFactory = dataRepositoryFactory;
            _productsChangesProcessor = productsChangesProcessor;
            _appAppSettingsManager = appSettingsManager;
            _serverDataManager = serverDataManager;
            _serverConnectionsFactory = serverConnectionsFactory;
            _dataParsersFactory = dataParsersFactory;
            //   _downloader = downloader;
        }

        public void Cancel()
        {
            if (Cts != null)
            {
                Cts.Cancel();
             //   Cts.Token.ThrowIfCancellationRequested();
            }
        }

        public void Start()
        {
          GetProducts();

            if (_appAppSettingsManager.CurrentSettings.UseAutodownloadUpdates)
            {
                //_downloader.Download()
            }


            //var v8Connector =  _serverConnectionsFactory.GetServerConnection<IServerConnectionV8>();
            //v8Connector.Authorization();

            // var parser = _dataParsersFactory.GetDataParser<ISummaryPageParser>();

            //  var serverProductsCollection = _serverDataManager.GetServerProducts(_appSettingsManager.ServerSettings);
            //
            //if (serverProductsCollection.HasError){
            //    //processerrors
            //}

            //      _productsChangeManager.Process(_dataRepositoryFactory,  _comparer, serverProductsCollection);

        }


        private bool SetConnection(ServerConnectionBase serverConnection, Uri BaseUri)
        {

            bool hasRespond = false;
            int connectionAttemptsCount = 0;

            var perdiodicTask = PeriodicTaskFactory<bool>.Start(
                () =>
                {
                    connectionAttemptsCount++;

                    if (connectionAttemptsCount > 1)
                    {
                        ProcessNotificatorText.SendNotification(new ProcessNotification(String.Format("������� � {0} ����������� � �������",
                            connectionAttemptsCount)));
                    }

                    hasRespond = serverConnection.IsUrlHasRespond(BaseUri);

                    return hasRespond;

                }, 10000, 100, -1, serverConnectionAttempts, true, Cts.Token, TaskCreationOptions.None, true);

            try
            {
                perdiodicTask.Wait(Cts.Token);
            }
            catch (AggregateException ex)
            {
                ex.Flatten();
                throw;
            }

            bool Authorized = false;

            if (hasRespond)
            {
                Authorized = serverConnection.Authorization();
                
                if (!Authorized)
                {
                    ProcessNotificatorText.SendNotification(
                        new ProcessNotification("����� ��� ������ �������"));
                    Thread.Sleep(5000);
                }
              
            }


            return hasRespond && Authorized;
        }
           

        public void GetProducts()
        {
            _appAppSettingsManager.LoadSettings();

            var settings = _appAppSettingsManager.CurrentSettings;

            if (_appAppSettingsManager.CurrentSettings.UseV8ProductsCheck)
            {
                ProcessNotificatorText.SendNotification(new ProcessNotification("��������� ���������� � �������� ���������� releases.1c.ru"));

                var v8Connection = _serverConnectionsFactory.GetServerConnection<IServerConnectionV8>();
                var a = new AuthDataSource(settings.V8AuthorizationSettings.Login,
                    settings.V8AuthorizationSettings.Password, V8LoginPage, V8BaseUri, V8ProductsTotalPage);

                v8Connection.SetAuthorizationDataSource(a);

                SetConnection(v8Connection as ServerConnectionBase, V8BaseUri);

                ProcessNotificatorText.SendNotification(new ProcessNotification("��������� ������ � ������� releases.1c.ru"));

                var v8ServerProducts =
                    _serverDataManager.GetServerProducts(v8Connection,
                        _dataParsersFactory.GetDataParser<ISummaryPageParserV8>(), "�������");

                ProcessNotificatorText.SendNotification(new ProcessNotification("��������� ������..."));

                _productsChangesProcessor.Process(_dataRepositoryFactory, v8ServerProducts);
            }

            if (_appAppSettingsManager.CurrentSettings.UseV7ProductsCheck)
            {
                ProcessNotificatorText.SendNotification(new ProcessNotification("��������� ���������� � �������� ���������� techsupp.1c.ru"));

                IServerConnectionV7 v7Connection = _serverConnectionsFactory.GetServerConnection<IServerConnectionV7>();

                v7Connection.SetAuthorizationDataSource(new AuthDataSource(settings.V7AuthorizationSettings.Login,
                    settings.V7AuthorizationSettings.Password, V7LoginPage, V7BaseUri, V7ProductsTotalPage));

                SetConnection(v7Connection as ServerConnectionBase, V7BaseUri);

                ProcessNotificatorText.SendNotification(new ProcessNotification("��������� ������ � ������� techsupp.1c.ru"));

                var v7ServerProducts =
                    _serverDataManager.GetServerProducts(v7Connection,
                        _dataParsersFactory.GetDataParser<ISummaryPageParserV7>(), "������");
                
                _productsChangesProcessor.Process(_dataRepositoryFactory, v7ServerProducts);

                ProcessNotificatorText.SendNotification(new ProcessNotification("��������� ������..."));
            }

            ProcessNotificatorText.SendNotification(new ProcessNotification("�������� ��������� ���������� ���������"));
        }

        public IEnumerable<Product> GetAllProducts()
        {
            var products = _dataRepositoryFactory.GetDataRepository<IProductsRepository>();

            return products.Get();
        }

        public bool IsDbEmpty()
        {
            var products = _dataRepositoryFactory.GetDataRepository<IProductsRepository>();

            return !products.Get().Any();
        }

        public IEnumerable<ProductDTO> GetAllProductsWithVersions()
        {
            var products = _dataRepositoryFactory.GetDataRepository<IProductsRepository>();

            return products.GetAllProductsWithVersions();
        }
    }

    public class EngineBase
    {
        protected CancellationTokenSource Cts;
        public IProcessNotificator ProcessNotificatorText { get; set; }

        public EngineBase()
        {
            Cts = new CancellationTokenSource();
            ProcessNotificatorText = new ProcessNotificator();
        }
    }
}
    
