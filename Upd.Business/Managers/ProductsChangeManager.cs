using System;
using System.Collections.Generic;
using System.Linq;
using Core.Common;
using Core.Common.Contracts;
using Upd.Business.Contracts;
using Upd.Business.Entities;
using Upd.Data.Contracts;
using Version = Upd.Business.Entities.Version;

namespace Upd.Business.Managers
{
    public class ProductsChangesProcessor : IProductsChangesProcessor
    {
        private List<Product> productsForUpdate;
        private List<Version> versionsForUpdate;

        private List<VersionChange> versionChanges;
        private List<ProductChange> productChanges;

        private IList<Product> dbProducts;

        private IDataRepositoryFactory dataRepositoryFactory;

        public void Process(IDataRepositoryFactory dataRepositoryFactory,
            ICollection<Product> serverProductsCollection)
        {

            Initialize(dataRepositoryFactory);

            foreach (var dbProduct in dbProducts)
            {
                ///////
                var found = (from product in serverProductsCollection
                             where product.ProductCode == dbProduct.ProductCode
                             select product).FirstOrDefault();

                if (found == null) //not found product on server
                {
                    ProductNotFoundOnServer(dbProduct);
                    continue;
                }

                var changeType = ProcessProductData(dbProduct, found);

                if (changeType == ProductChangesEvent.NotAvailable)
                    continue;

                ProcessVersionsCompareDbSrv(dbProduct, found.Versions);

            }

            foreach (var srvProduct in serverProductsCollection)
            {
                var dbPrd = dbProducts.FirstOrDefault(x => x.ProductCode == srvProduct.ProductCode);

                if (dbPrd == null) //not found product on db
                {
                    ProductNotFoundOnDb(srvProduct);
                    continue;
                }

                var changeType = ProcessProductData(dbPrd, srvProduct);

                if (changeType != ProductChangesEvent.NotAvailable)
                {
                    ProcessVersionsCompareDbSrv(dbPrd, srvProduct.Versions);
                }
            }

            ApplyChanges();
        }

        private void ApplyChanges()
        {
            var productsRepo = dataRepositoryFactory.GetDataRepository<IProductsRepository>();
            var versionsRepository = dataRepositoryFactory.GetDataRepository<IVersionsRepository>();
            var productChangesRepo = dataRepositoryFactory.GetDataRepository<IProductChangesRepository>();
            var versionChangesRepo = dataRepositoryFactory.GetDataRepository<IVersionChangesRepository>();

            productsRepo.InsertOrUpdate(productsForUpdate);
            versionsRepository.InsertOrUpdate(versionsForUpdate);

            productChangesRepo.InsertOrUpdate(productChanges);
            versionChangesRepo.InsertOrUpdate(versionChanges);
        }

        private void Initialize(IDataRepositoryFactory repositoryFactory)
        {
            dataRepositoryFactory = repositoryFactory;

            versionsForUpdate = new List<Version>();
            productsForUpdate = new List<Product>();

            productChanges = new List<ProductChange>();
            versionChanges = new List<VersionChange>();

            var prRepo = repositoryFactory.GetDataRepository<IProductsRepository>();
            var productsDto = prRepo.GetAllProductsWithVersions();

            dbProducts = new List<Product>();

            foreach (var dto in productsDto)
            {
                var product = new Product();
                SimpleMapper.PropertyMap(dto, product);
                dbProducts.Add(product);
            }
        }

        private void ProcessVersionsCompareDbSrv(Product dbProduct, IList<Version> srvVersionsOfProduct)
        {
            var chekedSrvVersionsNumber = new List<string>();

            var dbVersionsOfProduct = dbProduct.Versions;

            foreach (var dbVersion in dbVersionsOfProduct)
            {
                var found = (from version in srvVersionsOfProduct
                             where version.VersionNumber == dbVersion.VersionNumber && version.VersionType == dbVersion.VersionType
                             select version).FirstOrDefault();

                if (found == null)
                {
                    dbVersion.IsActual = false;
                    versionsForUpdate.Add(dbVersion);
                    continue;
                }

                if (found.Date != dbVersion.Date)
                {
                    dbVersion.IsActual = true;
                    dbVersion.Date = found.Date;
                    versionsForUpdate.Add(dbVersion);
                    versionChanges.Add(new VersionChange(dbVersion.VersionId, dbVersion.VersionType, VersionChangesEvents.DateChanged, DateTime.Now));

                }

                chekedSrvVersionsNumber.Add(found.VersionNumber);
            }

            foreach (var srvVersion in srvVersionsOfProduct)
            {
                if (!chekedSrvVersionsNumber.Contains(srvVersion.VersionNumber))
                {
                    srvVersion.IsActual = true;
                    versionsForUpdate.Add(srvVersion);
                    versionChanges.Add(new VersionChange(srvVersion.VersionId, srvVersion.VersionType,
                        VersionChangesEvents.Added, DateTime.Now));
                }
            }
        }

        private ProductChangesEvent ProcessProductData(Product dbProduct, Product srvProduct)
        {
            var change = CompareProduct(dbProduct, srvProduct);

            if (change == ProductChangesEvent.NotAvailable)
            {
                dbProduct.IsAvailable = false;
                productsForUpdate.Add(dbProduct);
                productChanges.Add(new ProductChange(dbProduct.ProductId, ProductChangesEvent.NotAvailable,
                    DateTime.Now));

            }
            if (change == ProductChangesEvent.IsAvailable)
            {
                dbProduct.IsAvailable = true;
                productsForUpdate.Add(dbProduct);
                productChanges.Add(new ProductChange(dbProduct.ProductId, ProductChangesEvent.IsAvailable,
                     DateTime.Now));
            }

            return change;
        }

        ProductChangesEvent CompareProduct(Product dbProduct, Product srvProduct)
        {
            if (!dbProduct.IsAvailable & srvProduct.IsAvailable)
                return ProductChangesEvent.IsAvailable;
            if (dbProduct.IsAvailable & !srvProduct.IsAvailable)
                return ProductChangesEvent.NotAvailable;

            return ProductChangesEvent.Unchanged;
        }

        private void ProductNotFoundOnServer(Product dbProduct)
        {
            dbProduct.IsAvailable = false;
            productsForUpdate.Add(dbProduct);
        }

        private void ProductNotFoundOnDb(Product srvProduct)
        {
            productsForUpdate.Add(srvProduct);
          
            foreach (var version in srvProduct.Versions)
            {
                version.IsActual = true;
                versionsForUpdate.Add(version);
            }
        }
    }
}