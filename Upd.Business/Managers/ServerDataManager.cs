﻿using System;
using System.Collections.Generic;
using Core.Common.Contracts;
using Upd.Business.Contracts;
using Upd.Business.Entities;
using Upd.Business.Settings;
using Upd.Parser.Contracts;
using Upd.WebConnection.Contracts;

namespace Upd.Business.Managers
{
    public class ServerDataManager : IServerDataManager
    {
        public bool HasErrors { get; set; }

        public ICollection<Product> GetServerProducts(IServerConnection serverConnection, ISummaryPageParser parser, string validationKey)
        {

            var isc = serverConnection.Authorization();

            var data = serverConnection.GetProductsTotalPage();

            var parsedData = parser.Parse(data, validationKey);
       
            return parsedData;
        }
    }
}
