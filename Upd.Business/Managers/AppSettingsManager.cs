﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Upd.Business.Settings;

namespace Upd.Business.Managers
{
    public class AppSettingsManager : SettingsProviderBase<SettingsGroup>, IAppSettingsManager
    {

        public SettingsGroup CurrentSettings { get; set; }

        public AppSettingsManager() 
            : base(@"D:\\sets.xml")
        {
            CurrentSettings = new SettingsGroup();
        }

        

        public void LoadSettings()
        {
            if(File.Exists(SettingsFilepath))
             CurrentSettings = Load();
            
        }

        public void SaveSettings()
        {
            Save(CurrentSettings);
        }

      
    }

    
}
