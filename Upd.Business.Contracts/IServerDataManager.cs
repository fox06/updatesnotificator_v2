using System.Collections.Generic;
using Core.Common.Contracts;
using Upd.Business.Entities;
using Upd.Parser.Contracts;

namespace Upd.Business.Contracts
{
    public interface IServerDataManager
    {
        ICollection<Product> GetServerProducts(IServerConnection connectionsFactory, ISummaryPageParser parser,
            string validationKey);
        bool HasErrors { get; set; }
    }
}