﻿using System.Collections.Generic;
using Core.Common.Contracts;
using Upd.Business.Entities;

namespace Upd.Business.Contracts
{
    public interface IProductsChangesProcessor
    {
        void Process(IDataRepositoryFactory dataRepositoryFactory, ICollection<Product> serverProductsCollection);
    }
}