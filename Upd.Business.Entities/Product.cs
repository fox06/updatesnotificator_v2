﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Common.Contracts;

namespace Upd.Business.Entities
{
    public class Product : IIdentifiableEntity
    {
        public string ProductCode { get; set; }
        public string Name { get; set; }
        public Platform Platform { get; set; }
        public bool IsAvailable { get; set; }
        public bool CheckForUpdates { get; set; }


        public IList<Version> Versions { get; set; }


        public Product()
        {
            ProductId = Guid.NewGuid();
            Name = string.Empty;
            Platform = Platform.V8;
            IsAvailable = true;
            CheckForUpdates = true;
            Versions = new List<Version>();
       }

        public Guid EntityId
        {
            get { return ProductId; }
            set { ProductId = value; }
        }

        public Guid ProductId { get; set; }

        public void AddVersion(Version version)
        {
           version.ProductId = ProductId;
           Versions.Add(version);
        }

        public override string ToString()
        {
            return ProductCode + "; " + Name + "; " + Platform + "; " + IsAvailable + "; " + CheckForUpdates + "; " +
                   EntityId;
        }
    }
}

   
