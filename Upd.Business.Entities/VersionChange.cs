﻿using System;
using Core.Common.Contracts;

namespace Upd.Business.Entities
{
    public class VersionChange : IIdentifiableEntity
    {
        public Guid VesionId { get; set; }
        public VersionTypes VersionType { get; set; }
        public VersionChangesEvents ChangeEvent { get; set; }
        public DateTime? EventDate { get; set; }

        public VersionChange(Guid vesionId, VersionTypes versionType, VersionChangesEvents changeEvent, DateTime eventDate) : this()
        {
            EntityId = Guid.NewGuid();
            VesionId = vesionId;
            VersionType = versionType;
            ChangeEvent = changeEvent;
            EventDate = eventDate;
        }

        public VersionChange()
        {
            VersionchangeId = Guid.NewGuid();
        }

        public Guid EntityId
        {
            get { return VersionchangeId; }
            set { VersionchangeId = value; }
        }

        public Guid VersionchangeId { get; set; }
    }
}