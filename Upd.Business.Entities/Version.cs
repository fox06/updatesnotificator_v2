using System;
using Core.Common.Contracts;

namespace Upd.Business.Entities
{
    public class Version : IIdentifiableEntity
    {
        public Guid ProductId { get; set; }

        public string VersionNumber { get; set; }
        public DateTime? Date { get; set; }
        public VersionTypes VersionType { get; set; }
        public bool IsActual { get; set; }

        public Version(Guid productId, string versionNumber, VersionTypes versionType, DateTime date) : this()
        {
            EntityId = Guid.NewGuid();
            ProductId = productId;
            VersionNumber = versionNumber;
            VersionType = versionType;
            Date = date;
        }

        public Version()
        {
            VersionId = Guid.NewGuid();
            IsActual = true;
        }

        public Guid EntityId
        {
            get { return VersionId; }
            set { VersionId = value; }
        }
      
        public Guid VersionId { get; set; }

        public override string ToString()
        {
            return VersionNumber + "; " + VersionType + "; " + Date + "; " + ProductId + "; " + VersionId;
        }
    }
}