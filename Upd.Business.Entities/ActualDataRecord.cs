﻿using System;
using Core.Common.Contracts;

namespace Upd.Business.Entities
{
    public class ActualDataRecord : IIdentifiableEntity
    {
        public Guid ProductId { get; set; }
        public ProductChangesEvent ChangeEvent { get; set; }
        public DateTime? EventDate { get; set; }

        public ActualDataRecord(Guid productId, ProductChangesEvent changeEvent, DateTime eventDate)
            : this()
        {
            EntityId = Guid.NewGuid();
            ProductId = productId;
            ChangeEvent = changeEvent;
            EventDate = eventDate;
        }

        public ActualDataRecord()
        {
            ProductChangeId = Guid.NewGuid();
        }


        public Guid EntityId
        {
            get { return ProductChangeId; }
            set { ProductChangeId = value; }
        }
        public Guid ProductChangeId { get; set; }
    }
}