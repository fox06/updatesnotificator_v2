namespace Upd.Business.Entities
{
    public enum ProductChangesEvent
    {
        Unchanged,
        NotAvailable,
        IsAvailable
    }
}