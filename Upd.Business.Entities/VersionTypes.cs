namespace Upd.Business.Entities
{
    public enum VersionTypes
    {
        Release,
        Planned,
        Preview
    }
}