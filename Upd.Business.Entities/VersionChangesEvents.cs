﻿namespace Upd.Business.Entities
{
    public enum VersionChangesEvents
    {
        Added,
        Removed,
        DateChanged
    }
}