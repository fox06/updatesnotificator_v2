﻿using System;

namespace Core.Common.Contracts
{
    public interface IIdentifiableEntity
    {
        Guid EntityId { get; set; }
    }
}