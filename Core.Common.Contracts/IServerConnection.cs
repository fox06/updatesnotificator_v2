﻿namespace Core.Common.Contracts
{
    public interface IServerConnection
    {
        bool Authorization();
        string GetProductsTotalPage();

    }
}
