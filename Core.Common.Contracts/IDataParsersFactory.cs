namespace Core.Common.Contracts
{
    public interface IDataParsersFactory
    {
        T GetDataParser<T>() where T : IDataParser;
    }
}