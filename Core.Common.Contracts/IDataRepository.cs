﻿using System;
using System.Collections.Generic;

namespace Core.Common.Contracts
{
    public interface IDataRepository<T> : IDataRepository
        where T : IIdentifiableEntity, new()
    {
        T Add(T entity);

        void InsertOrUpdate(IEnumerable<T> entities);

        void Remove(T entity);

        void Remove(Guid id);

        T Update(T entity);

        IEnumerable<T> Get();

        T Get(Guid id);
    }



    public interface IDataRepository { }
}