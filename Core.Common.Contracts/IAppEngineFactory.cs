﻿namespace Core.Common.Contracts
{
    public interface IAppEngineFactory
    {
        T GetAppEngine<T>() where T : IAppEngine;
    }
}