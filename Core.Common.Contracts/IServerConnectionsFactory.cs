namespace Core.Common.Contracts
{
    public interface IServerConnectionsFactory
    {
        T GetServerConnection<T>() where T : IServerConnection;
    }
}