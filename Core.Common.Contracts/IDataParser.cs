﻿namespace Core.Common.Contracts
{
    public interface IDataParser<T> : IDataParser
    {
        T Parse(string htmlPage, string validationKey);
    }


    public interface IDataParser { }

}